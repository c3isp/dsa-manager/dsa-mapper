package it.cnr.iit.c3isp.dsamapper.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecursiveTest {

	public static void main(String[] args) {
		try {
			// TODO Auto-generated method stub
			// String collapsed="if  @1   then  can [?X_8, Read, ?X_9]";
			// String collapsed="if  @1 and @2  then  can [?X_8, Read, ?X_9]";
			// String
			// collapsed="if  @1 and @2 and @3 then  can [?X_8, Read, ?X_9]";
			// String collapsed = "if  @1 or @2  then  can [?X_8, Read, ?X_9]";
			// String collapsed =
			// "   if  @1 or @2  or @3 or @4   or    @5 then  can [?X_8, Read, ?X_9]";
			String collapsed = " if  @1 or @2  and @3 and @4 and @5 or @6 or @7     then  can [?X_8, Read, ?X_9]";
			System.out.println(getCollapsedLogicPart(collapsed));
			System.out.println(getCollapsedActionPart(collapsed));
			System.out
					.println(AttemptSemanticsRecursion(getCollapsedLogicPart(collapsed)));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String AttemptSemanticsRecursion(String collapsedlogicapart)
			throws Exception {

		/*
		 * 
		 * if string=@n return converted apply
		 */
		collapsedlogicapart = collapsedlogicapart.trim();
		if (collapsedlogicapart.startsWith("@")
				&& (collapsedlogicapart.length() < 5)) {
			return getApplyTerm(collapsedlogicapart); // this is just a term to
														// convert
		}

		if (isThisJustanOR(collapsedlogicapart)) {
			return constructOr(collapsedlogicapart);

		}

		String exp = "((?<=^)|(?<=and\\s)).+?(?:(?=\\sand\\s|$))";
		Pattern pattern = Pattern.compile(exp);

		Matcher matcher = pattern.matcher(collapsedlogicapart);

		int count = 0;
		String finalAND = "";
		while (matcher.find()) {

			String thisAndoperand = matcher.group().trim();

			if (count == 0) {

				finalAND = AttemptSemanticsRecursion(thisAndoperand);
			} else {
				finalAND = finalAND + " AND "
						+ AttemptSemanticsRecursion(thisAndoperand);
			}
			count++;
		}
		/*
		 * Trova qualsiasi cosa in maniera non greedy fino a che non trovi uno
		 * spazio and spazio oppure la fine della frase ma questo tutto, deve
		 * essere preceduto o dall'inizio dellafrase oppure da uno spazio and
		 * spazio
		 * 
		 * HO CAPITO
		 */
		//
		if (count == 0) {
			throw new Exception(
					"Looklike this was the entire complex rule in recursion,"
							+ " but i can't find any suitable operand for AND. "
							+ collapsedlogicapart);
		}
		return finalAND;
	}

	private static String constructOr(String orstring) throws Exception {

		Pattern pattern = Pattern.compile("@(\\d){1,3}");

		Matcher matcher = pattern.matcher(orstring);

		int count = 0;
		String finalOR = "";
		while (matcher.find()) {

			String thisTermSymbol = matcher.group().trim();
			if (count == 0) {

				finalOR = getApplyTerm(thisTermSymbol);
			} else {
				finalOR = finalOR + " OR " + getApplyTerm(thisTermSymbol);
			}
			count++;
		}
		if (count == 0)
			throw new Exception("constructOr can't find any symbol @n");

		return finalOR;
	}

	private static boolean isThisJustanOR(String rule) throws Exception {

		// inizio stringa,chiocciola,1 fino a 3 numeri, un numero da 1 a inf di
		// ( spazio, or, spazio, chiocciola con numero), fine stringa
		// ^@(\d){1,3}((\s)or\s@(\d){1,3}){1,}$
		Pattern pattern = Pattern
				.compile("^@(\\d){1,3}((\\s)or\\s@(\\d){1,3}){1,}$");

		Matcher matcher = pattern.matcher(rule);

		int count = 0;

		while (matcher.find()) {

			// String trimmed = matcher.group().trim();
			count++;
		}

		if (count == 1)
			return true; // just 1 match, it's perfect this is an OR
		if (count == 0)
			return false;// no match at all, this is not an OR
		throw new Exception("Something really bad during OR check. rule: "
				+ rule);
	}

	private static String getApplyTerm(String simbol) {

		return "{Apply of " + simbol + "}";
	}

	private static String getCollapsedLogicPart(String rule) throws Exception {

		int ifpos = rule.indexOf("if ");
		if (ifpos < 0)
			throw new Exception("can't find IF in rule: " + rule);
		int thenpos = rule.indexOf(" then ");
		if (thenpos < 0)
			throw new Exception("can't find THEN in rule: " + rule);
		String logic = rule.substring(ifpos + 3, thenpos);
		logic = logic.replaceAll("\\s+", " ");
		return logic.trim();

	}

	private static String getCollapsedActionPart(String rule) throws Exception {

		int br1 = rule.indexOf("[");
		if (br1 < 0)
			throw new Exception("can't find [ in rule: " + rule);
		int br2 = rule.indexOf("]");
		if (br2 < 0)
			throw new Exception("can't find ] in rule: " + rule);
		String actionpart = rule.substring(br1, br2 + 1);
		actionpart = actionpart.replaceAll("\\s+", " ");
		return actionpart.trim();
	}
}
