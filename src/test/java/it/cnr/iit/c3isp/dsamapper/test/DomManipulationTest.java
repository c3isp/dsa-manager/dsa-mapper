package it.cnr.iit.c3isp.dsamapper.test;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.cnr.iit.c3isp.dsamapper.mapper.CommonUtils;

public class DomManipulationTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * 
		 * <ob:ObligationsSet> <ob:Obligation> <ob:TriggersSet>
		 * <upol:TriggerRuleEvaluated FulfillOn="Permit" /> </ob:TriggersSet>
		 * <ob:ActionLog /> </ob:Obligation> </ob:ObligationsSet>
		 */
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document document = db.newDocument();

			Element os = document.createElement("ObligationsSet");
			Element o = document.createElement("Obligation");
			Element ts = document.createElement("TriggersSet");
			Element te = document.createElement("TriggerRuleEvaluated");
			te.setAttribute("FulfillOn", "Permit");
			Element a = document.createElement("ActionLog");

			ts.appendChild(te);
			o.appendChild(ts);
			o.appendChild(a);
			os.appendChild(o);

			System.out.println(CommonUtils.convertDomNodeToString(os));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
