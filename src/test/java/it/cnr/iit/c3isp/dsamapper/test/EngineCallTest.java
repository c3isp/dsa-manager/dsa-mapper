package it.cnr.iit.c3isp.dsamapper.test;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.cnr.iit.c3isp.dsamapper.mapper.DatumManager;
import it.cnr.iit.c3isp.dsamapper.mapper.MapperDirector;
import it.cnr.iit.c3isp.dsamapper.mapper.configjson.ConfigJsonManager;

public class EngineCallTest {

	public static void main(String[] args) {

		testMapDSA();
	}

	public static void testMapDSA() {
		// TODO Auto-generated method stub
		InputStream is = null;
		try {
			is = EngineCallTest.class.getClassLoader().getResourceAsStream(
					"testsounasega.xml");
			if (is == null) {
				throw new Exception("Could not find dsa.");// asd

			}
			String StringFromInputStream = IOUtils.toString(is, "UTF-8");

			long startTime = System.currentTimeMillis();
			String mappedDSA = MapperDirector.getMappedDsa(StringFromInputStream);
			System.out.println("******* Mapped DSA ********");
			System.out.println(mappedDSA);
			String upol = MapperDirector.getUPOL(StringFromInputStream,
					"DPO-UP");

			/*
			 * String mapperDSA = MapperDirector
			 * .getMappedDsa(StringFromInputStream);
			 */

			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.println(elapsedTime);

			System.out.println("******* UPOL ********");
			System.out.println(upol);
		} catch (Exception ex) {

			ex.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);
		}
	}

	/*public static void testMapDatum() {

		// TODO Auto-generated method stub
		try {
			InputStream is = EngineCallTest.class.getClassLoader()
					.getResourceAsStream(
							"DSA-applicationdomain-isp.xml");
			if (is == null) {
				throw new Exception("Could not find dsa.");// asd

			}
			ConfigJsonManager.BootstrapConfig();
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(is);

			// get the first element
			Element element = doc.getDocumentElement();

			// get all child nodes
			NodeList nodes = element.getChildNodes();

			// print the text content of each child
			for (int i = 0; i < nodes.getLength(); i++) {
				Node thisnode = nodes.item(i);
				if (thisnode.getNodeName().equalsIgnoreCase("data")) {
					// String cont = thisnode.getTextContent();
					DatumManager.MapThisDSADatum(thisnode);
				}
			}
		} catch (Exception ex) {

			ex.printStackTrace();
		}

	}*/
}
