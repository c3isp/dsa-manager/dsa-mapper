package it.cnr.iit.c3isp.dsamapper.test;



import java.io.IOException;

import it.cnr.iit.c3isp.dsamapper.mapper.restclient.DSARestClient;

public class DSARestClientTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String endpoint="https://dsamgrc3isp.iit.cnr.it:8443/DSAAPI";
		
		try {
			String dsa=DSARestClient.FetchDSAFromID("DSA-internaltest01",endpoint);
			System.out.println(dsa);
			boolean res=DSARestClient.UploadMappedDSA2(dsa, "DSA-internaltest01",endpoint);
			if(!res){
				throw new Exception("Fail to upload dsa");
			}
			res=DSARestClient.ChangeDSAStatus("DSA-internaltest01","MAPPED",endpoint);
			
			if(!res){
				throw new Exception("Fail to upload dsa");
				}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
