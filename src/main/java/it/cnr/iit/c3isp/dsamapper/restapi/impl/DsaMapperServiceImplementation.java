package it.cnr.iit.c3isp.dsamapper.restapi.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.c3isp.dsamapper.mapper.ConfigManager;
import it.cnr.iit.c3isp.dsamapper.mapper.ConfigManagerv2;
import it.cnr.iit.c3isp.dsamapper.mapper.MapperDirector;
import it.cnr.iit.c3isp.dsamapper.mapper.MapperException;
import it.cnr.iit.c3isp.dsamapper.mapper.restclient.DSARestClient;



@ApiModel(value = "DSA Mapper", description = "DSA Mapper REST APIs")
@RestController
@RequestMapping("/v1")
public class DsaMapperServiceImplementation {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local
	 * path, it can be in the java classpath or set as an environment variable
	 */
	@Value("${setting1}")
	private String setting1;

	@Value("${setting2}")
	private String setting2;
	
	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;
	
	

	@Autowired
	private ConfigManagerv2 conf;
	
	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;
        
        @Value("${rest.endpoint.dsaapi}")
        private String dsaapi_endpoint;

	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    	return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }
	
	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(DsaMapperServiceImplementation.class);

	/**
	 * Sample POST method
	 * 
	 * @param pathParam
	 *            a parameter extracted from path like /template/PARAM_VALUE/ it
	 *            has to match the {param} from the path
	 * @param headerdata
	 *            extracted from the path
	 * @param data
	 *            is extracted from the request payload
	 * @return just a string
	 */
	
	@ApiOperation(value = "Test retrival values from ConfigServer with v2(Mirko solution autowired)")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Error"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.GET, value = "/mapper/testcomfigserverv2")
	public ResponseEntity<String> testcomfigserverv2() {

		try {
			String endp = conf.getDsaapi_endpoint();
			return new ResponseEntity<String>(endp, HttpStatus.OK);
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Exception during operations: "
					+ e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	
	}
	
	@ApiOperation(value = "Test retrival values from ConfigServer with v1(singleton)")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Error"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.GET, value = "/mapper/testcomfigserverv1")
	public ResponseEntity<String> testcomfigserverv1() {

		try {
			return new ResponseEntity<String>(ConfigManager.getInstance().getDsaApiEndpoint(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>("Exception during operations: "
					+ e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}
	
	
	
	@ApiOperation(value = "Fetch unmapped DSA from DSA Store API, map every rule and append the mapped rule to appropriate element in the XML. After it's completed, the mapped DSA is uploaded back to repository through DSA Store API. In the end it changes the DSA status to MAPPED, through the DSA Manager Gateway API (update status).")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Error"),
			@ApiResponse(code = 404, message = "DSA not found"),
			@ApiResponse(code = 200, message = "MAPPED") })
	@RequestMapping(method = RequestMethod.GET, value = "/mapper/MapDSAByID/{dsaid}")
	public ResponseEntity<String> MapDSAByID(@PathVariable("dsaid") String dsaID) {

		

		try {
			String dsa = DSARestClient.FetchDSAFromID(dsaID,conf.getDsaapi_endpoint());
			if (dsa == null) {
				LOGGER.error("dsa not found: " + dsa);

				return new ResponseEntity<String>("DSA not found",
						HttpStatus.NOT_FOUND);
			}
			String mappedDSA = MapperDirector.getMappedDsa(dsa);
			boolean res = DSARestClient.UploadMappedDSA2(mappedDSA, dsaID,conf.getDsaapi_endpoint());
			if (!res) {
				throw new Exception("Fail to upload dsa");
			}
			if (MapperDirector.allgood)
				res = DSARestClient.ChangeDSAStatus(dsaID, "MAPPED",conf.getDsaapi_endpoint());
			else
				res = DSARestClient.ChangeDSAStatus(dsaID, "WARNING",conf.getDsaapi_endpoint());

			if (!res) {
				throw new Exception("Fail to upload dsa");
			}

		} catch (Exception e) {
			LOGGER.error("Exception during operations: " + e.getMessage());
			return new ResponseEntity<String>("Exception during operations: "
					+ e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

		}
		if (MapperDirector.allgood)

			return new ResponseEntity<String>("MAPPED", HttpStatus.OK);
		else
			return new ResponseEntity<String>("WARNING :"
					+ MapperDirector.ProblemsString, HttpStatus.OK);

		// return thisresponse;

	}

	@ApiOperation(value = "Fetch mapped (or unmapped) DSA via DSA Store API and build a standard UPOL or otherData UPOL depending on the dsaType parameter. Return UPOL XML in the body response. (In case of unmapped DSA, the DSA is mapped before proceeding).")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Error"),
			@ApiResponse(code = 404, message = "DSA not found"),
			@ApiResponse(code = 200, message = "UPOL in the body") })
	@RequestMapping(method = RequestMethod.POST, value = "/mapper/buildUPOL/{dsaid},{dsaType}")
	public ResponseEntity<String> buildUPOL(
			@ApiParam(value = "Dsa ID in the repository without .xml", required = true) @PathVariable("dsaid") String dsaID,
			@ApiParam(value = "Dsa Type: 1)DPO-UP for standard upol. 2)DO-P for otherData upol. 3)DPO-UP-DERIVED for standard derived upol. 4)DO-P-DERIVED for otherData derived upol.", required = true) @PathVariable("dsaType") String dsaType){
		try {
                        LOGGER.info("=== conf endpoint = " + conf.getDsaapi_endpoint());
                        LOGGER.info("=== dsaapi value = " + this.dsaapi_endpoint);
                        
			String dsa = DSARestClient.FetchDSAFromID(dsaID,conf.getDsaapi_endpoint());
			if (dsa == null) {
				LOGGER.error("dsa not found: " + dsa);

				return new ResponseEntity<String>("DSA not found",
						HttpStatus.NOT_FOUND);
			}
		
			String UPOL = MapperDirector.getUPOL(dsa, dsaType);
		
			 
			return new ResponseEntity<String>(UPOL, HttpStatus.OK);
		} catch (MapperException e) {
			LOGGER.error("Exception during operations: " + e.getMessage());
			return new ResponseEntity<String>("Exception during operations: "
					+ e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

/*

	@RequestMapping(method = RequestMethod.POST, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@PathVariable("param") String pathParam,
			@RequestHeader(value = "template-header-param") String headerdata, @RequestBody() Structure data) {
		LOGGER.info("I received a POST request");
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(HttpStatus.OK);
		return thisresponse;
	}

	
	@RequestMapping(method = RequestMethod.PUT, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@PathVariable("param") String pathParam,
			@RequestHeader(value = "template-header-param") String headerdata, @RequestBody() Structure data) {
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(HttpStatus.OK);
		return thisresponse;
	}


	@ApiOperation(httpMethod = "GET", value = "Get the resource",
			notes = "Return a json based on the Structure class. SpringBoot takes care of the transformation of the returned type the interface")
	@RequestMapping(method = RequestMethod.GET, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "X-myheader-attr1", value = "X-myheader-attr1", required = false, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "X-myheader-attr2", value = "X-myheader-attr2", required = false, dataType = "string", paramType = "header")
      })
	public ResponseEntity<Structure> get(@PathVariable("param") String param, HttpServletRequest hreq) {
		Structure responsedata = new Structure();
		responsedata.setId("anIdValue");
		responsedata.setName("aName");
		responsedata.setPath("aPath");
		responsedata.setVersion("1");

		return new ResponseEntity<Structure>(responsedata, HttpStatus.OK);
	}

	
	@ApiOperation(httpMethod = "DELETE", value = "Delete the resource")
	@RequestMapping(method = RequestMethod.DELETE, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@PathVariable("object") String objectId) {
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(HttpStatus.OK);
		return thisresponse;
	}


	// provides a description
	@ApiOperation(httpMethod = "HEAD", value = "Head the resource", notes = "Return the metadata associated with an object")
	// provides a documentation of the different http error messages and their
	// meaning from the application perspective
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Client Error"),
			@ApiResponse(code = 404, message = "Param not found"),
			@ApiResponse(code = 200, message = "Returned some headers") })
	@RequestMapping(method = RequestMethod.HEAD, value = "/template/{param}/")
	public ResponseEntity<String> head(@PathVariable("param") String param) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("template-header-1", setting1);
		responseHeaders.set("template-header-2", setting2);
		ResponseEntity<String> thisresponse = new ResponseEntity<String>(responseHeaders, HttpStatus.OK);
		return thisresponse;
	}
	

	@ApiOperation(httpMethod = "GET", value = "Call Get method",
			notes = "See Get method")
	@RequestMapping(method = RequestMethod.GET, value = "/calltemplate/{param}/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Structure> callGet(@PathVariable("param") String param, HttpServletRequest hreq) {
	
		MultiValueMap<String, String> mapParams = new LinkedMultiValueMap<String, String>();
		mapParams.add("param", param);
		Structure structure = restTemplate.getForObject(callGetEndpoint, Structure.class, mapParams);
		
		ResponseEntity<Structure> thisresponse = new ResponseEntity<Structure>(structure, HttpStatus.OK);
		return thisresponse;
	}
	
	
	*/
}
