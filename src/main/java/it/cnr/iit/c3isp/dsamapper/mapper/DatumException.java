/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper;

/**
 *
 * @author Vincenzo Farruggia
 */
public class DatumException extends MapperException {
    DatumException(String message) {
        super(message);
    }
}
