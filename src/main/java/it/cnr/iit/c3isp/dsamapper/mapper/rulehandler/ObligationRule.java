package it.cnr.iit.c3isp.dsamapper.mapper.rulehandler;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.wso2.balana.utils.policy.dto.ApplyElementDTO;
import org.wso2.balana.utils.policy.dto.ConditionElementDT0;
import org.wso2.balana.utils.policy.dto.RuleElementDTO;
import org.wso2.balana.utils.policy.dto.TargetElementDTO;

import it.cnr.iit.c3isp.dsamapper.mapper.CommonUtils;
import it.cnr.iit.c3isp.dsamapper.mapper.DatumException;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.AbstractRule.RuleType;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import org.w3c.dom.DOMException;
import org.wso2.balana.utils.exception.PolicyBuilderException;

public class ObligationRule extends AbstractRule {
	final static Logger log = LogManager.getLogger();

	public ObligationRule(Node thisnode, RuleType ruletypew, String purposew,
			String dataclassificationw) throws Exception {

		log.debug("creating new Obligation ");

		this.internalState = InternalState.NOT_VALID_SKIP;
		this.original_dom_parent_element = (Element) thisnode;
		this.purpose = purposew;
		this.dataClassification = dataclassificationw;
		this.ruleType = ruletypew;
		this.finalMappedRule = getXACMLMappedRule();
		this.internalState = InternalState.VALID_MAPPED;

	}

	@Override
	protected RuleElementDTO getXACMLMappedRule() throws RuleException, DatumException {
		log.debug("converting Obligation..");
		if (original_dom_parent_element == null)
			throw new RuleException("no original_cnl_rule_dom");

		initCnlFromDomElement();

		if (containsDMO()) {
			RuleElementDTO mappedRule = convertObligationWithDMO();
			return mappedRule;

		}
		RuleElementDTO mappedRule = new RuleElementDTO();
		

		if (this.collapseTerms()) {

			// prendi la parte target
			// prendi la parte conditionale
			// se la regola e' permit fai permit, senno fai deny, metti il nome
			// alla regola.

			ConditionElementDT0 cond = createXACMLConditionPart(true);

			String tripleteAction = getCollapsedActionPart(collapsedTerms_rule_string);
			String action = getMatchActionFromActionPart(tripleteAction);

			TargetElementDTO tar = createXACMLTargetActionPart(action);
			mappedRule.setConditionElementDT0(cond);
			mappedRule.setTargetElementDTO(tar);
			mappedRule.setRuleEffect("Permit");
			mappedRule.setRuleId(this.NameOftheRule);
		} else {
			log.debug(
					"Collapsed term return false in Obligation. so nothing to collapse. Maybe a rule with can referring to the system or malformed dsa. ie. can [Subject,delete,Data] must be if hasId etc. Im creating a basic rule form instead of skip and crossfingers.");
			String tripleteAction = getCollapsedActionPart(collapsedTerms_rule_string);
			String action = getMatchActionFromActionPart(tripleteAction);
			TargetElementDTO tar = createXACMLTargetActionPart(action);
			mappedRule.setConditionElementDT0(createXACMLConditionPart(false));

			mappedRule.setTargetElementDTO(tar);
			mappedRule.setRuleEffect("Permit");
			mappedRule.setRuleId(this.NameOftheRule);
		}
		return mappedRule;
	}

	private RuleElementDTO convertObligationWithDMO() throws RuleException, DatumException {

		// <expression issuer="Legal Expert" language="CNL4DSA">after [?X_6,
		// Write, ?X_7] then must [?X_8, Log, ?X_9]</expression>

		log.debug("Converting special case dmo obligation..");

		RuleElementDTO mappedRule = new RuleElementDTO();
                log.debug(" original_dom_parent_element = " + original_dom_parent_element.toString());

		String statinfo = original_dom_parent_element.getAttribute("statementInfo");

		if (statinfo.length() > 3) { 
                    // 3 semplicemente per evitare la stringa vuota o il def value
                    
                    log.debug("Ok the rule has statmentInfo, lets create an Obligation");
                    log.debug("Content of statement info: " + statinfo);
                    try {
                        mappedRule.setObligationElementDTOs(this.getObligationStatmentinfo(statinfo));
                        log.debug("Obligation added.");
                    } catch (UnsupportedEncodingException ex) {
                        log.error("Error getting statement info: " + ex.getMessage());
                    }
		}
		
		//TODO da modifcare in caso di multiple DMO
		int poscurlbracket=statinfo.indexOf("{");
		String dmo_name=statinfo.substring(0,poscurlbracket);
		
		ConditionElementDT0 cond=null;
		if (this.collapseTerms()) {
			log.debug("collapseTerms() returned true in special case obli dmo.");
			 cond = createXACMLConditionPart(true);
			
		}
		
		else {
			log.debug("collapseTerms() returned false in special case obli dmo.");

			 cond = createXACMLConditionPart(false);

		}
		
		
		String matchTargetAction = getTargetActionInPresenceOFDMO(original_cnl_rule_string,dmo_name);
		
		
		TargetElementDTO tar = createXACMLTargetActionPart(matchTargetAction);

		mappedRule.setConditionElementDT0(cond);
		mappedRule.setTargetElementDTO(tar);
		mappedRule.setRuleEffect("Permit");
		mappedRule.setRuleId(this.NameOftheRule);

		this.internalState = InternalState.VALID_MAPPED;

		return mappedRule;
	}
	
	private String getTargetActionInPresenceOFDMO(String orig_rule,String dmo_name) throws RuleException {
            log.debug("Trying to get the correct action given the dmo is: " + dmo_name);


            int search_position=0;
            Set<String> list_inside_bracket= new HashSet<String>();

            while(orig_rule.indexOf("[",search_position)!=-1) {

                    int curlpos=orig_rule.indexOf("[",search_position);
                    int comma1pos=orig_rule.indexOf(",",curlpos);
                    int comma2pos=orig_rule.indexOf(",",comma1pos+1);
                    String dmo_or_action=orig_rule.substring(comma1pos+1, comma2pos);
                    list_inside_bracket.add(dmo_or_action.trim().toLowerCase());
                    search_position=comma2pos;

            }
            list_inside_bracket.remove(dmo_name.trim().toLowerCase());

            if(list_inside_bracket.size()!=1) {
                    throw new RuleException("list or dmos_action is not 1. We are not left with only the action, dunno whats going on.");
            }

            Iterator<String> itr = list_inside_bracket.iterator();
	    while(itr.hasNext()){
	        return itr.next();
	    }
            
            throw new RuleException("Cant find the single action inside list?????????????????????? Should not happen at all.");

	}

	private boolean containsDMO() {

		if (this.original_cnl_rule_string.contains("after")) {
			return true;
		} else
			return false;
	}

	@Override
	public Element getDomFinalRule() throws RuleException {
		// TODO Auto-generated method stub
		if (this.internalState != InternalState.VALID_MAPPED) {

			throw new RuleException(
					"this Rule it's not valid...how do you can get DOM?");
		}

		String obliAction = "";
		if (containsDMO()) {

			obliAction = getObliAction(this.original_cnl_rule_string);

		} else {
			obliAction = getObliAction(this.collapsedTerms_rule_string);
		}
                try {
                    Element standardRule = CommonUtils.convertXacmlRuleElementToElement(
                                    finalMappedRule,
                                    this.original_dom_parent_element.getOwnerDocument());
                    /*Element obligationPart = createObligationPart(obliAction,
                                    this.original_dom_parent_element.getOwnerDocument());

                    standardRule.appendChild(obligationPart);*/

                    CommonUtils.adjustDecisionTimeAndNsAndPatchPasteONGOING(standardRule,
                                    this);

                    return standardRule;
                } catch(PolicyBuilderException e) {
                    throw new RuleException("Error converting xacml rule to element: " + e.getMessage());
                }

	}

	private String getObliAction(String rule) throws RuleException {

		int mustInd = rule.indexOf("must");
		int br1 = rule.indexOf(",", mustInd);
		int br2 = rule.indexOf(",", br1 + 1);
		if (mustInd < 0 || br1 < 0 || br2 < 0 || (br2 - br1) < 3) {
			throw new RuleException("Cant find obligation Action");
		}
		String action = rule.substring(br1 + 1, br2);
		return action.trim();
	}

	private Element createObligationPart(String action, Document document)
			throws DOMException {

		Element os = document.createElement("ObligationsSet");
		Element o = document.createElement("Obligation");
		Element ts = document.createElement("TriggersSet");

		if (this.isContinousRule) {

			Element t1 = document.createElement("TriggerRuleEvaluated");
			t1.setAttribute("FulfillOn", "EndAccess");
			Element t2 = document.createElement("TriggerRuleEvaluated");
			t1.setAttribute("FulfillOn", "Revoke");
			ts.appendChild(t1);
			ts.appendChild(t2);
		} else {
			Element t1 = document.createElement("TriggerRuleEvaluated");
			t1.setAttribute("FulfillOn", "Permit");
			ts.appendChild(t1);
		}

		o.appendChild(ts); // <TriggersSet>
		// <TriggerRuleEvaluated FulfillOn="Permit"/>
		// </TriggersSet>

		if (action.equalsIgnoreCase("log")) {
			Element a = document.createElement("ActionLog");
			o.appendChild(a);
		} else if (action.equalsIgnoreCase("delete")) {
			Element a = document.createElement("ActionDelete");
			o.appendChild(a);
		} else {

			log.warn("At this point i have no clue about meaning of action while creating obligationset. But instead of exception, i will create a default carrying obligation with ID same as found.");
			Element a = document.createElement(action);
			o.appendChild(a);
		}

		os.appendChild(o);

		return os;
	}

}
