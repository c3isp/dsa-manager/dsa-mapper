package it.cnr.iit.c3isp.dsamapper.restapi.types;

public class ResponseType {
	  String id;
	    String mappeddsa;

	    public ResponseType(String id, String dsa) {
	        this.id = id;
	        this.mappeddsa = dsa;
	    }

	    public String getId() {
	        return this.id;
	    }

	    public String getMappeddsa() {
	        return this.mappeddsa;
	    }
}
