package it.cnr.iit.c3isp.dsamapper.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.cnr.iit.c3isp.dsamapper.mapper.configjson.ConfigJsonManager;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.AbstractRule;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.AuthProibRule;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.ObligationRule;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.RuleException;
import javax.xml.transform.TransformerException;

public final class MapperDirector {
	final static Logger log = LogManager.getLogger();

	public static boolean allgood = true;
	public static String ProblemsString = "";

	private MapperDirector() {
	}

	// static private Document lastDSADoc;
	static private List<AbstractRule> allrule = new ArrayList<AbstractRule>();

	static private Document lastDSA = null;

	// static private Document thisDSADoc = null;
	public static String getDsaVersion() throws MapperException {

		Element dsaEle = lastDSA.getDocumentElement();
		String dsaversion = dsaEle.getAttribute("version");
		if (dsaversion == null || dsaversion.isEmpty()) {
			throw new MapperException("Could not find DSA Version in dsa document");

		}
		return dsaversion;
	}

	public static String getMappedDsa(String DSAtoMap) throws MapperException, TransformerException, RuleException {

		Document docDSA_Untouched = parseDsaAndMap(DSAtoMap);
		lastDSA = docDSA_Untouched;
		Element MappedDsaFirstEle = generateDsa(docDSA_Untouched);
		return CommonUtils.convertDomNodeToString(MappedDsaFirstEle);

	}

	public static String getUPOL(String DSA, String upolType) throws MapperException {

		// (DPO-P) - DPO Usage Policy (DPO-UP)
		Document docDSA_Untouched = parseDsaAndMap(DSA);
		lastDSA = docDSA_Untouched;
		String DSAID = getDSAID(docDSA_Untouched);

		
		if (upolType.equalsIgnoreCase("DPO-UP")) {
			return UPOLBuilder.createUPOL(DSAID, allrule, false,false);
			
		} else if (upolType.equalsIgnoreCase("DO-P")) {
			return UPOLBuilder.createUPOL(DSAID, allrule, true,false);	
			
		}else if (upolType.equalsIgnoreCase("DPO-UP-DERIVED")) {
			return UPOLBuilder.createUPOL(DSAID, allrule, false,true);
			
		}else if (upolType.equalsIgnoreCase("DO-P-DERIVED")) {
			return UPOLBuilder.createUPOL(DSAID, allrule, true,true);
		} else {
			throw new MapperException("Unknown UPOL type");
		}
	}

	private static Element generateDsa(Document dsaDoc) throws MapperException, RuleException, TransformerException {

		for (AbstractRule ar : allrule) {

			Element OriginalDOMRuleParent = ar.getOriginal_dom_parent_element();

			Element finalRule = ar.getDomFinalRule();

			Node cdata = dsaDoc.createCDATASection(CommonUtils
					.convertDomNodeToString(finalRule));
			Element cln = CommonUtils
					.FindDeep_FirstChildElementByTagAndAttribute(
							OriginalDOMRuleParent, "expression", "language",
							"UPOL");
			if (cln == null) {
				throw new MapperException("UPOL node not found in generateDSA.");
			}
			CommonUtils.removeAllChilds(cln);
			cln.appendChild(cdata);
		}
		return dsaDoc.getDocumentElement();
	}

	private static Document parseDsaAndMap(String dsa) throws MapperException, DatumException {
		allrule.clear();
		allgood = true;
		ProblemsString = "";
                                
                try {
                    ConfigJsonManager.BootstrapConfig();
                } catch(Exception e) {
                    throw new MapperException("Error while bootstrapping json config");
                }

                Document thisDSADoc;
                
                try {
                    // MapperDirector.lastDSADoc = doc;
                    thisDSADoc = CommonUtils.getDSADoc(dsa);
                } catch(Exception e) {
                    throw new MapperException("Error parsing DSA: " + dsa);
                }

		// get the first element
		Element element = thisDSADoc.getDocumentElement();

		// first of all take the dsa attribute: purpose
		String purpose = element.getAttribute("purpose");
		if (purpose == null || purpose.isEmpty()) {
			throw new MapperException(
					"Can't find DSA purpose, we can't create rules xacml.. Abort");
		}

		
	
		String appDomainString = element.getAttribute("application-domain");
		if (appDomainString == null || appDomainString.isEmpty()) {
			log.info("Cant find application-domain, leaving it empty..");
			appDomainString="";
		}
		
		
		
		
		
		
		UPOLBuilder.setApplicationDomain(appDomainString);
		// get the validity date to create future upol
		Node validtyNode = CommonUtils.findFirstLevelChildByTagName(element,
				"validity");

		if (validtyNode == null) {

			throw new MapperException("Cant find validity tag, cant go ahead.");
		}

		Node startDate = CommonUtils.findFirstLevelChildByTagName(validtyNode,
				"startDate");
		Node endDate = CommonUtils.findFirstLevelChildByTagName(validtyNode,
				"endDate");

		if (startDate == null || endDate == null) {
			throw new MapperException(
					"Cant find startDate or Enddate in validty tag. cant go ahead.");
		}
		String startDateText = startDate.getTextContent();
		String endDateText = endDate.getTextContent();

		if (startDateText == null || endDateText == null) {
			throw new MapperException(
					"Cant find startDate or Enddate in validty tag. cant go ahead.");
		}

		log.debug("validity startdate:" + startDateText + " enddate:"
				+ endDateText);

		UPOLBuilder.setValidity_EndDate(endDateText);
		UPOLBuilder.setValidity_StartDate(startDateText);

		// expiration Stuff
		Node expirationNode = CommonUtils.findFirstLevelChildByTagName(element,
				"expirationPolicy");

		if (expirationNode == null) {

			throw new MapperException("Cant find expirationNode tag, cant go ahead.");
		}

		String expirationDays = ((Element) expirationNode)
				.getAttribute("periodInDays");

		if (expirationDays.isEmpty())
			throw new MapperException("Cant find expirationDays, cant go ahead.");

		Node actionexpirationNode = CommonUtils.findFirstLevelChildByTagName(
				expirationNode, "complexPolicy");

		if (actionexpirationNode == null) {
			throw new MapperException(
					"Cant find action for Expiration. cant go ahead.");
		}

		String expirationAction = ((Element) actionexpirationNode)
				.getTextContent();

		if (expirationAction.isEmpty() || expirationAction.length() < 3)
			throw new MapperException("Cant find expirationAction, cant go ahead.");

		UPOLBuilder.setExpiration_days(expirationDays);
		UPOLBuilder.setExpiration_action(expirationAction);

		// revocation part
		Node revocationNode = CommonUtils.findFirstLevelChildByTagName(element,
				"revocationPolicy");

		if (revocationNode == null) {

			throw new MapperException("Cant find revocationNode tag, cant go ahead.");
		}

		String revocationDays = ((Element) revocationNode)
				.getAttribute("periodInDays");

		if (revocationDays.isEmpty())
			throw new MapperException("Cant find revocationDays, cant go ahead.");

		Node actionrevocationNode = CommonUtils.findFirstLevelChildByTagName(
				revocationNode, "complexPolicy");

		if (actionrevocationNode == null) {
			throw new MapperException(
					"Cant find action for revocation. cant go ahead.");
		}

		String revocationAction = ((Element) actionrevocationNode)
				.getTextContent();

		if (revocationAction.isEmpty() || revocationAction.length() < 3)
			throw new MapperException("Cant find revocationAction, cant go ahead.");

		UPOLBuilder.setRevocation_days(revocationDays);
		UPOLBuilder.setRevocation_action(revocationAction);

		
		
		// update stuff
		Node updateNode = CommonUtils.findFirstLevelChildByTagName(element,
				"updatePolicy");

		if (updateNode == null) {

			throw new MapperException("Cant find updateNode tag, cant go ahead.");
		}

		String updateDays = ((Element) updateNode).getAttribute("periodInDays");

		if (updateDays.isEmpty())
			throw new MapperException("Cant find updateDays, cant go ahead.");

		Node actionupdateNode = CommonUtils.findFirstLevelChildByTagName(
				updateNode, "complexPolicy");

		if (actionupdateNode == null) {
			throw new MapperException("Cant find action for update. cant go ahead.");
		}

		String updateAction = ((Element) actionupdateNode).getTextContent();

		if (updateAction.isEmpty() || updateAction.length() < 3)
			throw new MapperException("Cant find updateAction, cant go ahead.");

		UPOLBuilder.setUpdate_days(updateDays);
		UPOLBuilder.setUpdate_action(updateAction);

		// the we must find the dataClassification. We can't wait to parse
		// everything
		// we need this to create the rules (the AND inside Condition

		Node nodedataClassification = CommonUtils.findFirstLevelChildByTagName(
				element, "dataClassification");
		if (nodedataClassification == null) {
			throw new MapperException(
					"Can't find dataClassification element in DSA, we can't keep goind cause we can't create rules xacml..Abort.");

		}

		String dataClassification = nodedataClassification.getTextContent();
		if (dataClassification == null || dataClassification.isEmpty()) {
			throw new MapperException(
					"Can't find text inside dataClassification element in DSA, we can't keep goind cause we can't create rules xacml..Abort.");

		}

		NodeList nodes = element.getChildNodes();

		for (int i = 0; i < nodes.getLength(); i++) {
			Node thisnode = nodes.item(i);
			String NodeName = thisnode.getNodeName();
                        log.debug("examining node=" + NodeName);
                        log.debug(" -- nodeContent = " + thisnode.getTextContent());
                        
			if (NodeName.equalsIgnoreCase("data")) {
				DatumManager.MapThisDSADatum(thisnode);
			}

			if (NodeName.equalsIgnoreCase("authorizations")) {
				log.debug("authorizations tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.AUTH, purpose,
						dataClassification,false);
			}
			if (NodeName.equalsIgnoreCase("dataSubjectAuthorizations")) {
				log.debug("dataSubjectAuthorizations tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.AUTH, purpose,
						dataClassification,false);
			}

			if (NodeName.equalsIgnoreCase("prohibitions")) {
				log.debug("prohibitions tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.PROIBH,
						purpose, dataClassification,false);
			}

			if (NodeName.equalsIgnoreCase("obligations")) {
				log.debug("obligations tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.OBLIG, purpose,
						dataClassification,false);
			}

			if (NodeName.equalsIgnoreCase("derivedObjectsObligations")) {
				log.debug("derivedObjectsObligations tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.OBLIG, purpose,
						dataClassification,true);
			}
			if (NodeName.equalsIgnoreCase("derivedObjectsProhibitions")) {
				log.debug("derivedObjectsProhibitions tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.PROIBH,
						purpose, dataClassification,true);
			}
			if (NodeName.equalsIgnoreCase("derivedObjectsAuthorizations")) {
				log.debug("derivedObjectsAuthorizations tree found");
				mapThisRuleTree(thisnode, AbstractRule.RuleType.AUTH, purpose,
						dataClassification,true);
			}
		}

		return thisDSADoc;
	}

	public static void mapThisRuleTree(Node treeElement,
			AbstractRule.RuleType ruletype, String purpose,
			String dataclassification, boolean derived) {

		// get all child nodes
		NodeList nodes = treeElement.getChildNodes();
		String nodeName = null;
                
                log.debug("mapThisRuleTree " + treeElement.getTextContent());
                
		// print the text content of each child
		for (int i = 0; i < nodes.getLength(); i++) {
			try {
				Node thisnode = nodes.item(i);
				if (thisnode.getNodeType() != Node.ELEMENT_NODE) {
                                    // not interest in non element node (text,comment ecc)
                                    continue;
				}
				nodeName = thisnode.getNodeName();
				nodeName = nodeName.trim();
				if (nodeName.isEmpty())
					continue;
				if (nodeName.equalsIgnoreCase("authorization")
						&& ruletype == AbstractRule.RuleType.AUTH) {

					log.debug("Found an auth mapping...");
					AuthProibRule thisRule = new AuthProibRule(thisnode,
							ruletype, purpose, dataclassification);
					thisRule.setDerived(derived);
					// CommonUtils.debugAbstractRule(thisRule);
					MapperDirector.allrule.add(thisRule);

				} else if (nodeName.equalsIgnoreCase("prohibition")
						&& ruletype == AbstractRule.RuleType.PROIBH) {

					log.debug("Found a prohibition mapping...");
					AuthProibRule thisRule = new AuthProibRule(thisnode,
							ruletype, purpose, dataclassification);
					thisRule.setDerived(derived);

					// CommonUtils.debugAbstractRule(thisRule);
					MapperDirector.allrule.add(thisRule);

				} else if (nodeName.equalsIgnoreCase("obligation")
						&& ruletype == AbstractRule.RuleType.OBLIG) {
					ObligationRule thisobli = new ObligationRule(thisnode,
							ruletype, purpose, dataclassification);
					thisobli.setDerived(derived);

					MapperDirector.allrule.add(thisobli);
				} else {
					log.warn("WTF is this? :) nodeName: "
							+ nodeName
							+ " ruleType claimed: "
							+ ((ruletype == AbstractRule.RuleType.AUTH) ? "auth"
									: ((ruletype == AbstractRule.RuleType.PROIBH) ? "proibh"
											: "obli?")));
				}

			} catch (Exception e) {
                            e.printStackTrace();
				allgood = false;
				ProblemsString = ProblemsString + "  " + e.getMessage();
				log.warn("Exception during understanding rule/node: "
						+ nodeName + " Reason: " + e.getMessage()
						+ " Continue with next....");
			} finally {
				// nothing, continue with other rules...
			}
		}
	}

	private static String getDSAID(Document doc) throws MapperException {

		Element dsaEle = doc.getDocumentElement();
		String dsaID = dsaEle.getAttribute("id");
		if (dsaID == null || dsaID.isEmpty()) {
			throw new MapperException("Could not find DSA ID in dsa document");

		}
		return dsaID;
	}
}
