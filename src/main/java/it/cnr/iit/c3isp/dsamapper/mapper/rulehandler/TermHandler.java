/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper.rulehandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.balana.utils.policy.dto.ApplyElementDTO;
import org.wso2.balana.utils.policy.dto.AttributeDesignatorDTO;
import org.wso2.balana.utils.policy.dto.AttributeValueElementDTO;

import it.cnr.iit.c3isp.dsamapper.mapper.CommonUtils;

/**
 *
 * @author root
 */
public class TermHandler {
	final static Logger log = LogManager.getLogger();

	public static enum TermType {
		GENERAL_CASE, GPS_CASE, HASID_CASE, TIME_ISGREATER, TIME_ISLOWER, HAS_ROLE, IS_RELATED,IS_MEMBER_OF,
		HAS_END_TIME_AFTER,HAS_END_TIME_BEFORE,HAS_START_TIME_AFTER,HAS_START_TIME_BEFORE,HAS_END_DATE_AFTER,
		HAS_START_DATE_AFTER,HAS_END_DATE_BEFORE,HAS_START_DATE_BEFORE
	}

	private TermType termtype;
	private String CodeName;

	public String getCodeName() {
		return CodeName;
	}

	private String CnlLabel;
	private String Chi_X;
	private String Cosa_x;
	private String Chi_resolved;
	private String Cosa_resolved;
	private boolean NottedAttribute;
	private String XACML_Category;
	private String XACML_ID;
	private boolean isContinous;
	private boolean isOpenFields; // this mean also this is a special case xacml
                                      // conversion
	private String OpenFieldValue;

	private ApplyElementDTO Apply_XACML;

	public TermHandler(String codeName, String cnlLabel, String chi_x,
			String cosa_x, String chi_resolved, String cosa_resolved,
			boolean notted, String categ, String id, boolean open,
			boolean continuous, String openfieldvaluep) throws Exception {

		this.CodeName = codeName;
		this.CnlLabel = cnlLabel;
		this.Chi_X = chi_x;
		this.Cosa_x = cosa_x;
		this.Chi_resolved = chi_resolved;
		this.Cosa_resolved = cosa_resolved;
		this.NottedAttribute = notted;
		this.XACML_Category = categ;
		this.XACML_ID = id;
		this.setContinous(continuous);
		this.isOpenFields = open;
		this.OpenFieldValue = openfieldvaluep;

		if (this.isOpenFields
				&& (OpenFieldValue == null || OpenFieldValue.isEmpty())) {

			throw new Exception(
					"Trying to construct Term that is openfield but"
							+ " hasn't openfield value..This sould not happen cause it's "
							+ "checke before this, but anyway..");
		}

		if (this.CnlLabel.equalsIgnoreCase("hasLocation")
				|| this.CnlLabel.equalsIgnoreCase("hasContinuousLocation")) {
			this.termtype = TermType.GPS_CASE;

		} else if (this.CnlLabel.equalsIgnoreCase("isGreater")
				|| this.CnlLabel.equalsIgnoreCase("isContinuousGreater")) {
			this.termtype = TermType.TIME_ISGREATER;

		} else if (this.CnlLabel.equalsIgnoreCase("isLower")
				|| this.CnlLabel.equalsIgnoreCase("isContinuousLower")) {
			this.termtype = TermType.TIME_ISLOWER;

		} else if (this.CnlLabel.equalsIgnoreCase("hasId")) {
			this.termtype = TermType.HASID_CASE;

		} else if (this.CnlLabel.equalsIgnoreCase("hasRole")) {
			this.termtype = TermType.HAS_ROLE;

		} else if (this.CnlLabel.equalsIgnoreCase("isRelatedTo")) {
			this.termtype = TermType.IS_RELATED;

		}
		else if (this.CnlLabel.equalsIgnoreCase("isMemberOf")) {
			this.termtype = TermType.IS_MEMBER_OF;

		}
		
		
		else if (this.CnlLabel.equalsIgnoreCase("hasEndTimeAfter")){
			this.termtype=TermType.HAS_END_TIME_AFTER;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasStartTimeAfter")){
			this.termtype=TermType.HAS_START_TIME_AFTER;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasEndTimeBefore")){
			this.termtype=TermType.HAS_END_TIME_BEFORE;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasStartTimeBefore")){
			this.termtype=TermType.HAS_START_TIME_BEFORE;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasEndDateAfter")){
			this.termtype=TermType.HAS_END_DATE_AFTER;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasStartDateAfter")){
			this.termtype=TermType.HAS_START_DATE_AFTER;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasEndDateBefore")){
			this.termtype=TermType.HAS_END_DATE_BEFORE;
		}
		else if (this.CnlLabel.equalsIgnoreCase("hasStartDateBefore")){
			this.termtype=TermType.HAS_START_DATE_BEFORE;
		}
		
		else {
			this.termtype = TermType.GENERAL_CASE;
		}

		convertThisTermInXacml();

	}

	public boolean convertThisTermInXacml() {

		switch (this.termtype) {
		case GENERAL_CASE:
			log.debug("Converting general case term..");
			convertGeneralCase();
			break;
		case GPS_CASE:
			log.debug("Converting GPS case term..");
			convertGPSCase();
			break;
		case IS_RELATED:
			log.debug("Converting isRelatedTo case term..");
			convertIsRelatedTo();
			break;
		case HASID_CASE:
			log.debug("Converting HasId case term..");
			convertHasIDCase();
			break;
		case TIME_ISGREATER:
			log.debug("Converting time is greater case term..");
			convertTimeisGreater();
			break;
		case TIME_ISLOWER:
			log.debug("Converting time is lower case term..");
			convertTimeisLower();
			break;
		case HAS_ROLE:
			log.debug("Converting hasrole case term..");
			convertHasRole();
			break;
		case IS_MEMBER_OF:
			log.debug("Converting isMemberOf case term..");
			convertIsMemberOf();
			break;

		case HAS_START_TIME_AFTER:
			log.debug("converting term: HAS_START_TIME_AFTER");
			convert_time("urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-start-time");
			break;
		case HAS_START_TIME_BEFORE:
			log.debug("converting term: HAS_START_TIME_BEFORE");
			convert_time("urn:oasis:names:tc:xacml:1.0:function:time-less-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-end-time");
			break;
		case HAS_END_TIME_AFTER:
			log.debug("converting term: HAS_END_TIME_AFTER");
			convert_time("urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-end-time");
			break;
		case HAS_END_TIME_BEFORE:
			log.debug("converting term: HAS_END_TIME_BEFORE");
			convert_time("urn:oasis:names:tc:xacml:1.0:function:time-less-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-end-time");
			break;
			
			
		case HAS_END_DATE_AFTER:
			log.debug("converting term: HAS_END_DATE_AFTER");
			convert_date("urn:oasis:names:tc:xacml:1.0:function:date-greater-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-end-date");
			break;
		case HAS_END_DATE_BEFORE:
			log.debug("converting term: HAS_END_DATE_BEFORE");
			convert_date("urn:oasis:names:tc:xacml:1.0:function:date-less-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-end-date");
			break;
		case HAS_START_DATE_AFTER:
			log.debug("converting term: HAS_START_DATE_AFTER");
			convert_date("urn:oasis:names:tc:xacml:1.0:function:date-greater-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-start-date");
			break;
		case HAS_START_DATE_BEFORE:
			log.debug("converting term: HAS_START_DATE_BEFORE");
			convert_date("urn:oasis:names:tc:xacml:1.0:function:date-less-than-or-equal","urn:oasis:names:tc:xacml:1.0:environment:data-end-date");
			break;

			
			
		default:
			return false;

		}
		return true;
	}

	private void convert_date(String functionID, String AttributeID) {
		ApplyElementDTO applytime = CommonUtils
				.createApplyDateWithFunctionID(					
						functionID,
						"urn:oasis:names:tc:xacml:3.0:attribute-category:environment",
						AttributeID,
						this.OpenFieldValue, this.NottedAttribute);
		this.setApply_XACML(applytime);
		return;
		
	}

	private void convert_time(String functionID, String AttributeID) {
		ApplyElementDTO applytime = CommonUtils
				.createApplyTimeWithFunctionID(
						functionID,
						"urn:oasis:names:tc:xacml:3.0:attribute-category:environment",
						AttributeID,
						this.OpenFieldValue, this.NottedAttribute);

		this.setApply_XACML(applytime);
		return;
		
	}

	private void convertIsMemberOf() {
		ApplyElementDTO ApplyStringEqual = new ApplyElementDTO();
		ApplyStringEqual
				.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:string-is-in");
		AttributeValueElementDTO attValue = new AttributeValueElementDTO();
		attValue.setAttributeDataType("http://www.w3.org/2001/XMLSchema#string");
		attValue.setAttributeValue(this.OpenFieldValue);

		AttributeDesignatorDTO des = CommonUtils.createDesignator(
				this.XACML_Category, this.XACML_ID,
				"http://www.w3.org/2001/XMLSchema#string");

		ApplyStringEqual.setAttributeValueElementDTO(attValue);
		ApplyStringEqual.setAttributeDesignators(des);

		if (this.NottedAttribute) {
			ApplyStringEqual = CommonUtils.notThisApply(ApplyStringEqual);
		}

		this.setApply_XACML(ApplyStringEqual);
		
	}

	private void convertIsRelatedTo() {

		ApplyElementDTO app1 = new ApplyElementDTO();
		app1.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:string-equal");

		ApplyElementDTO app2 = CommonUtils
				.createStringOneAndOnlyWithDesignatorApply(this.XACML_Category,
						this.XACML_ID);

		ApplyElementDTO app3 = CommonUtils
				.createStringOneAndOnlyWithDesignatorApply(
						"urn:oasis:names:tc:xacml:1.0:subject-category:access-subject",
						"urn:oasis:names:tc:xacml:1.0:subject:subject-id");

		app1.getApplyElements().add(app2);
		app1.getApplyElements().add(app3);

		if (this.NottedAttribute) {
			app1 = CommonUtils.notThisApply(app1);
		}

		this.setApply_XACML(app1);

	}

	private void convertHasRole() {

		ApplyElementDTO ApplyStringEqual = new ApplyElementDTO();
		ApplyStringEqual
				.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:string-is-in");
		AttributeValueElementDTO attValue = new AttributeValueElementDTO();
		attValue.setAttributeDataType("http://www.w3.org/2001/XMLSchema#string");
		attValue.setAttributeValue(this.Cosa_resolved);

		AttributeDesignatorDTO des = CommonUtils.createDesignator(
				this.XACML_Category, this.XACML_ID,
				"http://www.w3.org/2001/XMLSchema#string");

		ApplyStringEqual.setAttributeValueElementDTO(attValue);
		ApplyStringEqual.setAttributeDesignators(des);

		if (this.NottedAttribute) {
			ApplyStringEqual = CommonUtils.notThisApply(ApplyStringEqual);
		}

		this.setApply_XACML(ApplyStringEqual);

	}

	private void convertTimeisLower() {
		ApplyElementDTO applytime = CommonUtils
				.createApplyTimeWithFunctionID(
						"urn:oasis:names:tc:xacml:1.0:function:time-less-than-or-equal",

						"urn:oasis:names:tc:xacml:3.0:attribute-category:environment",
						"urn:oasis:names:tc:xacml:3.0:environment:currentTime",

						this.OpenFieldValue, this.NottedAttribute);


		this.setApply_XACML(applytime);
		return;
	}

	private void convertTimeisGreater() {

		ApplyElementDTO applytime = CommonUtils
				.createApplyTimeWithFunctionID(
						"urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal",
						"urn:oasis:names:tc:xacml:3.0:attribute-category:environment",
						"urn:oasis:names:tc:xacml:3.0:environment:currentTime",
						this.OpenFieldValue, this.NottedAttribute);



		this.setApply_XACML(applytime);
		return;

	}

	private void convertGeneralCase() {

		ApplyElementDTO ApplyTermXacml = null;
		if (this.isOpenFields) {
			ApplyTermXacml = CommonUtils.createApplyForStringEqualTest(
					this.XACML_Category, this.XACML_ID, this.OpenFieldValue);
		} else {
			ApplyTermXacml = CommonUtils.createApplyForStringEqualTest(
					this.XACML_Category, this.XACML_ID, this.Cosa_resolved);
		}

		if (this.NottedAttribute) {
			ApplyTermXacml = CommonUtils.notThisApply(ApplyTermXacml);
		}
		this.setApply_XACML(ApplyTermXacml);

		return;

	}

	private void convertHasIDCase() {
		ApplyElementDTO ApplyTermXacml = CommonUtils
				.createApplyForStringEqualTest(this.XACML_Category,
						this.XACML_ID, this.OpenFieldValue);
		if (this.NottedAttribute) {
			ApplyTermXacml = CommonUtils.notThisApply(ApplyTermXacml);
		}
		this.setApply_XACML(ApplyTermXacml);
		return;

	}

	private void convertGPSCase() {
		/*
		 * v<Apply
		 * FunctionId="urn:oasis:names:tc:xacml:3.0:function:Area-is-in">
		 * 
		 * <Apply
		 * FunctionId="urn:oasis:names:tc:xacml:1.0:function:string-one-and-only"
		 * > <AttributeDesignator
		 * AttributeId="urn:oasis:names:tc:xacml:3.0:subject:position"
		 * Category="urn:oasis:names:tc:xacml:1.0:subject-category:access-subject"
		 * DataType="http://www.w3.org/2001/XMLSchema#string"
		 * MustBePresent="true"></AttributeDesignator> </Apply>
		 * 
		 * <AttributeValue
		 * DataType="http://www.w3.org/2001/XMLSchema#string">POSIZIONEX
		 * ,POSIZIONEY,RAGGIO</AttributeValue>
		 * 
		 * </Apply>
		 */
		ApplyElementDTO ApplyStringEqual = new ApplyElementDTO();
		ApplyStringEqual
				.setFunctionId("urn:oasis:names:tc:xacml:3.0:function:area-in-range");

		AttributeValueElementDTO attValue = new AttributeValueElementDTO();
		attValue.setAttributeDataType("http://www.w3.org/2001/XMLSchema#string");
		attValue.setAttributeValue(this.OpenFieldValue);

		ApplyElementDTO stringOneAndOnlyApply = CommonUtils
				.createStringOneAndOnlyWithDesignatorApply(this.XACML_Category,
						this.XACML_ID);
		ApplyStringEqual.setApplyElement(stringOneAndOnlyApply);

		ApplyStringEqual.setAttributeValueElementDTO(attValue);

		if (this.NottedAttribute) {
			ApplyStringEqual = CommonUtils.notThisApply(ApplyStringEqual);
		}
		this.setApply_XACML(ApplyStringEqual);
		return;
	}

	/*
	 * private helper section
	 */

	public boolean isContinous() {
		return isContinous;
	}

	private void setContinous(boolean isContinous) {
		this.isContinous = isContinous;
	}

	public ApplyElementDTO getApply_XACML() {
		return Apply_XACML;
	}

	private void setApply_XACML(ApplyElementDTO apply_XACML) {
		Apply_XACML = apply_XACML;
	}
}
