/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper.configjson;

/**
 *
 * @author root
 */
public class AttributeJson {

	public String ATTR;
	public String CAT;
	public String ID;
	public boolean CONT;
	public boolean OPENFIELD;

	public AttributeJson() {
		this.CONT = false;
		this.OPENFIELD = false;
	}
}
