package it.cnr.iit.c3isp.dsamapper.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.wso2.balana.utils.exception.PolicyBuilderException;
import org.wso2.balana.utils.policy.dto.ApplyElementDTO;
import org.wso2.balana.utils.policy.dto.ConditionElementDT0;
import org.wso2.balana.utils.policy.dto.RuleElementDTO;

import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.AbstractRule;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.RuleException;
import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Serializer;
import nu.xom.converters.DOMConverter;

public class UPOLBuilder {
	final static Logger log = LogManager.getLogger();

	private static final Map<String, String> map_actionsExpUpRev = Collections
			.unmodifiableMap(new HashMap<String, String>() {
				{
					put("DenyAll", "DenyAll");
					put("DenyAllAndDeleteNow", "DenyAllAndDeleteNow");
					put("DeleteInSpecifiedPeriod", "DeleteInSpecifiedPeriod");
				}
			});
	/*
	 * <upol:Policy xmlns="https://cococloud.eu/ns/upol"
	 * xmlns:upol="https://cococloud.eu/ns/upol"
	 * xmlns:sp="http://www.primelife.eu/ppl/stickypolicy"
	 * xmlns:ob="http://www.primelife.eu/ppl/obligation"
	 * xmlns:ppl="http://www.primelife.eu/ppl"
	 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	 * xmlns:xacml="urn:oasis:names:tc:xacml:3.0:core:schema:wd-17"
	 * xsi:schemaLocation
	 * ="http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd"
	 * 
	 * PolicyId="DSA-29130219-59a3-4bb0-8107-768643761151" RuleCombiningAlgId=
	 * "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable"
	 * Version="3.0">
	 * 
	 * <xacml:Description>UPOL Policy</xacml:Description>
	 * 
	 * <upol:DSA_id>DSA-e08ed1e0-8e6d-478f-a0ba-f1d07ea79610</upol:DSA_id>
	 * 
	 * <xacml:Target/>
	 */
	private static String appDomain=null;
	private static org.w3c.dom.Document lastValiddoc = null;
	private static String validity_EndDate = "";

	public static String getValidity_EndDate() {
		return validity_EndDate;
	}

	public static void setValidity_EndDate(String validity_EndDate) {
		UPOLBuilder.validity_EndDate = validity_EndDate;
	}

	private static String validity_StartDate = "";

	public static String getValidity_StartDate() {
		return validity_StartDate;
	}

	public static void setValidity_StartDate(String validity_StartDate) {
		UPOLBuilder.validity_StartDate = validity_StartDate;
	}

	// Expiration part
	private static String expiration_days = "";

	public static String getExpiration_days() {
		return expiration_days;
	}

	public static void setExpiration_days(String expiration_days) {
		UPOLBuilder.expiration_days = expiration_days;
	}

	private static String expiration_action = "";

	public static String getExpiration_action() {
		return expiration_action;
	}

	public static void setExpiration_action(String expiration_action) {
		UPOLBuilder.expiration_action = expiration_action;
	}

	// Revocation part
	private static String revocation_days = "";

	public static String getRevocation_days() {
		return revocation_days;
	}

	public static void setRevocation_days(String revocation_days) {
		UPOLBuilder.revocation_days = revocation_days;
	}

	private static String revocation_action = "";

	public static String getRevocation_action() {
		return revocation_action;
	}

	public static void setRevocation_action(String revocation_action) {
		UPOLBuilder.revocation_action = revocation_action;
	}

	// Update part
	private static String update_days = "";

	public static String getUpdate_days() {
		return update_days;
	}

	public static void setUpdate_days(String update_days) {
		UPOLBuilder.update_days = update_days;
	}

	private static String update_action = "";

	public static String getUpdate_action() {
		return update_action;
	}

	public static void setUpdate_action(String update_action) {
		UPOLBuilder.update_action = update_action;
	}

	public final static String NSUpol = "https://cococloud.eu/ns/upol";
	public final static String NSSP = "http://www.primelife.eu/ppl/stickypolicy";
	public final static String NSOb = "http://www.primelife.eu/ppl/obligation";
	public final static String NSPpl = "http://www.primelife.eu/ppl";
	public final static String NSXsi = "http://www.w3.org/2001/XMLSchema-instance";
	public final static String NSXacml = "urn:oasis:names:tc:xacml:3.0:core:schema:wd-17";
	public final static String NSSchemaLoc = "http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd";

	static public Element getUPOLSkeleton(String DSAID) {

		String CombAlg = "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable";
		String Ver = "3.0";

		Element policyEle = new Element("upol:Policy", NSUpol);
		policyEle.addNamespaceDeclaration("sp", NSSP);
		policyEle.addNamespaceDeclaration("ob", NSOb);

		policyEle.addNamespaceDeclaration("ppl", NSPpl);

		policyEle.addNamespaceDeclaration("xsi", NSXsi);

		policyEle.addNamespaceDeclaration("xacml", NSXacml);

		policyEle.addNamespaceDeclaration("schemaLocation", NSSchemaLoc);

		policyEle.addAttribute(new Attribute("RuleCombiningAlgId", CombAlg));
		policyEle.addAttribute(new Attribute("version", Ver));

		// Namespace nss = new
		// Namespace("prefix","http://www.primelife.eu/ppl/obligation",
		// policyEle);

		// policyEle.addNamespaceDeclaration("xmlns:upol", NSUpol);
		/*
		 * policyEle.addAttribute(new Attribute("xmlns:upol", NSUpol));
		 * policyEle.addAttribute(new Attribute("xmlns:sp", NSSP));
		 * policyEle.addAttribute(new Attribute("xmlns:ob", NSOb));
		 * policyEle.addAttribute(new Attribute("xmlns:ppl", NSPpl));
		 * policyEle.addAttribute(new Attribute("xmlns:xsi", NSXsi));
		 * policyEle.addAttribute(new Attribute("xmlns:xacml", NSXacml));
		 * policyEle .addAttribute(new Attribute("xsi:schemaLocation",
		 * NSSchemaLoc));
		 */

		Element descEle = new Element("xacml:Description", NSXacml);
		descEle.appendChild("UPOL Policy");
		policyEle.appendChild(descEle);

		Element dsaEle = new Element("upol:DSA_id", NSUpol);
		String dsaSenzaxml = DSAID;
		if (DSAID.contains(".xml")) {
			dsaSenzaxml = DSAID.substring(0, DSAID.lastIndexOf(".xml"));
		}
		policyEle.addAttribute(new Attribute("PolicyId", dsaSenzaxml));
		dsaEle.appendChild(dsaSenzaxml);
		policyEle.appendChild(dsaEle);

		Element Targetele = new Element("xacml:Target", NSXacml);
		policyEle.appendChild(Targetele);

		return policyEle;

	}

	static private Element getDefaultDeny(Element skeletonupol)
			throws UPOLBuilderException {
		RuleElementDTO dr = new RuleElementDTO();
		dr.setRuleEffect("Deny");
		dr.setRuleId("default-deny");

		if (lastValiddoc == null) {
			throw new UPOLBuilderException("lastValidDoc null during def deny");
		}
                try {
                    org.w3c.dom.Element e = CommonUtils.convertXacmlRuleElementToElement(
                                    dr, lastValiddoc);
                    return DOMConverter.convert(e);
                } catch(PolicyBuilderException e) {
                    throw new UPOLBuilderException("Error while converting Rule to Element: " + e.getMessage());
                }

	}

	/*
	 * static private Element convertAbstractRuleToUPOLElement( AbstractRule
	 * orig_rule) throws Exception {
	 * 
	 * org.w3c.dom.Element ruleEle = orig_rule.getDomFinalRule();
	 * 
	 * Element ruleUPOL = DOMConverter.convert(ruleEle);
	 * 
	 * ruleUPOL.getChildElements();
	 * 
	 * return ruleUPOL; }
	 */

	static public String createUPOL(String DSAID, List<AbstractRule> rule_list,
			boolean protect, boolean derived) throws UPOLBuilderException, MapperException, RuleException {
		List<AbstractRule> orderedlist = getOrderedList(rule_list);

		Element skeletonupol = getUPOLSkeleton(DSAID);
		boolean firsttime = true;

		for (AbstractRule ar : orderedlist) {
			if (lastValiddoc == null) {
				lastValiddoc = ar.getDomFinalRule().getOwnerDocument();
			}
			if (firsttime && !protect) {
				skeletonupol.appendChild(DOMConverter.convert(CommonUtils
						.getDoubleProibhDSAAVAILABLE(lastValiddoc)));

				firsttime = false;
			}
			if (derived && !ar.isDerived()) continue;
			if(!derived && ar.isDerived()) continue;
			
                        if (!protect && ar.isProtected())
				continue;

			if (protect && !ar.isProtected())
				continue;

			skeletonupol
					.appendChild(DOMConverter.convert(ar.getDomFinalRule()));

		}
                try {
                    if(!protect) {
                        skeletonupol.appendChild(getRevokationStuff(skeletonupol));
                        skeletonupol.appendChild(getUpdateStuff(skeletonupol));
                        skeletonupol.appendChild(getExpirationStuff(skeletonupol));
                    }
                } catch(PolicyBuilderException e) {
                    throw new UPOLBuilderException("Error add element in skeleton upol: " + e.getMessage());
                }

		skeletonupol.appendChild(getDefaultDeny(skeletonupol));
		lastValiddoc = null;
		Document doc = new Document(skeletonupol);
		addNSToRulesXACML(doc);
		String docs=doc.toXML();
		docs=docs.replaceAll("<Match ", "<xacml:Match ");
		docs=docs.replaceAll("</Match>", "</xacml:Match>");
		return docs;

	}

	private static Element getUpdateStuff(Element skeletonupol)
			throws PolicyBuilderException {
		RuleElementDTO mappedRule = new RuleElementDTO();

		mappedRule.setRuleEffect("Deny");
		mappedRule.setRuleId("OBLIGATION_Update");

		ApplyElementDTO app = CommonUtils.createApplyForStringEqualTest(
				"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
				"urn:oasis:names:tc:xacml:3.0:resource:dsa-status", "UPDATED");
		ConditionElementDT0 cond = new ConditionElementDT0();
		cond.setApplyElement(app);
		mappedRule.setConditionElementDT0(cond);
		org.w3c.dom.Element e = CommonUtils.convertXacmlRuleElementToElement(
				mappedRule, lastValiddoc);

		org.w3c.dom.Element cond2 = CommonUtils
				.FindDeep_FirstChildElementByTagAndAttribute(e, "Condition",
						null, null);
		cond2.setAttribute("DecisionTime", "pre");

		org.w3c.dom.Element os = lastValiddoc.createElement("ObligationsSet");
		org.w3c.dom.Element o = lastValiddoc.createElement("Obligation");
		org.w3c.dom.Element ts = lastValiddoc.createElement("TriggersSet");
		org.w3c.dom.Element tpe = lastValiddoc
				.createElement("TriggerPolicyEvaluated");
		tpe.setAttribute("FulfillOn", "Deny");
		org.w3c.dom.Element max = lastValiddoc.createElement("MaxDelay");
		org.w3c.dom.Element dur = lastValiddoc.createElement("Duration");
		dur.setTextContent("P0Y0M" + update_days + "DT0H0M0S");
		org.w3c.dom.Element start = lastValiddoc.createElement("Start");

		org.w3c.dom.Element datetime = lastValiddoc
				.createElement("DateAndTime");
		datetime.setTextContent(getDateTimeObligation(update_days,
				validity_EndDate));

		org.w3c.dom.Element action = lastValiddoc
				.createElement(map_actionsExpUpRev.get(update_action));

		// create tree
		os.appendChild(o);
		o.appendChild(ts);
		ts.appendChild(tpe);
		tpe.appendChild(max);
		max.appendChild(dur);
		tpe.appendChild(start);
		start.appendChild(datetime);
		o.appendChild(action);

		e.appendChild(os);
		return DOMConverter.convert(e);

	}

	private static Element getRevokationStuff(Element skeletonupol)
			throws PolicyBuilderException {

		RuleElementDTO mappedRule = new RuleElementDTO();

		mappedRule.setRuleEffect("Deny");
		mappedRule.setRuleId("OBLIGATION_Revoke");

		ApplyElementDTO app = CommonUtils.createApplyForStringEqualTest(
				"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
				"urn:oasis:names:tc:xacml:3.0:resource:dsa-status", "REVOKED");
		ConditionElementDT0 cond = new ConditionElementDT0();
		cond.setApplyElement(app);
		mappedRule.setConditionElementDT0(cond);
		org.w3c.dom.Element e = CommonUtils.convertXacmlRuleElementToElement(
				mappedRule, lastValiddoc);

		org.w3c.dom.Element cond2 = CommonUtils
				.FindDeep_FirstChildElementByTagAndAttribute(e, "Condition",
						null, null);
		cond2.setAttribute("DecisionTime", "pre");

		org.w3c.dom.Element os = lastValiddoc.createElement("ObligationsSet");
		org.w3c.dom.Element o = lastValiddoc.createElement("Obligation");
		org.w3c.dom.Element ts = lastValiddoc.createElement("TriggersSet");
		org.w3c.dom.Element tpe = lastValiddoc
				.createElement("TriggerPolicyEvaluated");
		tpe.setAttribute("FulfillOn", "Deny");
		org.w3c.dom.Element max = lastValiddoc.createElement("MaxDelay");
		org.w3c.dom.Element dur = lastValiddoc.createElement("Duration");
		dur.setTextContent("P0Y0M" + revocation_days + "DT0H0M0S");
		org.w3c.dom.Element start = lastValiddoc.createElement("Start");

		org.w3c.dom.Element datetime = lastValiddoc
				.createElement("DateAndTime");
		datetime.setTextContent(getDateTimeObligation(revocation_days,
				validity_EndDate));

		org.w3c.dom.Element action = lastValiddoc
				.createElement(map_actionsExpUpRev.get(revocation_action));

		// create tree
		os.appendChild(o);
		o.appendChild(ts);
		ts.appendChild(tpe);
		tpe.appendChild(max);
		max.appendChild(dur);
		tpe.appendChild(start);
		start.appendChild(datetime);
		o.appendChild(action);

		e.appendChild(os);
		return DOMConverter.convert(e);

	}

	private static Element getExpirationStuff(Element skeletonupol)
			throws PolicyBuilderException {
		RuleElementDTO mappedRule = new RuleElementDTO();

		mappedRule.setRuleEffect("Deny");
		mappedRule.setRuleId("OBLIGATION_Expiration");

		org.w3c.dom.Element e = CommonUtils.convertXacmlRuleElementToElement(
				mappedRule, lastValiddoc);

		org.w3c.dom.Element os = lastValiddoc.createElement("ObligationsSet");
		org.w3c.dom.Element o = lastValiddoc.createElement("Obligation");
		org.w3c.dom.Element ts = lastValiddoc.createElement("TriggersSet");
		org.w3c.dom.Element tat = lastValiddoc.createElement("TriggerAtTime");
		org.w3c.dom.Element max = lastValiddoc.createElement("MaxDelay");
		org.w3c.dom.Element dur = lastValiddoc.createElement("Duration");
		dur.setTextContent("P0Y0M" + expiration_days + "DT0H0M0S");
		org.w3c.dom.Element start = lastValiddoc.createElement("Start");

		org.w3c.dom.Element datetime = lastValiddoc
				.createElement("DateAndTime");
		datetime.setTextContent(getDateTimeObligation(expiration_days,
				validity_EndDate));

		org.w3c.dom.Element action = lastValiddoc
				.createElement(map_actionsExpUpRev.get(expiration_action));

		// create tree
		os.appendChild(o);
		o.appendChild(ts);
		ts.appendChild(tat);
		tat.appendChild(max);
		max.appendChild(dur);
		tat.appendChild(start);
		start.appendChild(datetime);
		o.appendChild(action);

		e.appendChild(os);
		return DOMConverter.convert(e);

	}

	private static String getDateTimeObligation(String expiration_days2,
			String validity_EndDate2) {
		/*
		 * Alternatively, if your string were in ISO8601 format (that is,
		 * yyyy-MM-dd'T'HH:mm:ssZ), you could just use parse(String) instead:
		 * DateTime dateTime = DateTime.parse(dt1);
		 */
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-ddZZ");

		DateTime dateTime = DateTime.parse(validity_EndDate2, fmt);
		Integer day = Integer.parseInt(expiration_days2);
		DateTime dateTime2 = dateTime.plusDays((int) day);

		return dateTime2.toString();
	}

	private static void addNSToRulesXACML(Document doc) {
		// TODO Auto-generated method stub
		Node elroot = doc.getRootElement();
		recursiveAdjustNS(elroot);

	}

	private static final String[] xacmlTag = { "Apply", "Condition", "Rule",
			"Target", "AnyOf", "AllOf", "AttributeDesignator", "AttributeValue" };

	private static final String[] OBTag = { "ObligationsSet", "Obligation",
			"TriggersSet", "TriggerRuleEvaluated", "ActionLog", "ActionDelete" };

	private static void recursiveAdjustNS(Node el) {

		// recurse the children
		for (int i = 0; i < el.getChildCount(); i++) {
			recursiveAdjustNS(el.getChild(i));
		}

		if (el.getClass() == Element.class) {
			Element e = (Element) el;

			if (e.getNamespaceURI() == null || e.getNamespaceURI().isEmpty()) {

				String tag = e.getLocalName();
				if (Arrays.asList(xacmlTag).contains(tag)) {

					// e.setLocalName("xacml:" + tag);
					e.setNamespaceURI(NSXacml);
					e.setNamespacePrefix("xacml");

				} else if (Arrays.asList(OBTag).contains(tag)) {

					// e.setLocalName("xacml:" + tag);
					e.setNamespaceURI(NSOb);
					e.setNamespacePrefix("ob");

				}

			}
			/*
			 * System.out.println(); System.out.println(e.getLocalName() +
			 * " nsc:" + e.getNamespaceDeclarationCount() + " nsu:" +
			 * e.getNamespaceURI());
			 */

		}
	}

	static private List<AbstractRule> getOrderedList(
			List<AbstractRule> rule_list) {

		List<AbstractRule> OrderedRuleList = new ArrayList<AbstractRule>(
				rule_list);
		/*
		 * Comparator<AbstractRule> comparator =
		 * Comparator.comparing(AbstractRule->); comparator =
		 * comparator.thenComparing(Comparator.comparing(person -> person.age));
		 */
		Collections.sort(OrderedRuleList, new Comparator<AbstractRule>() {

			@Override
			public int compare(AbstractRule r1, AbstractRule r2) {

				if (r1.getIssuer().getNumVal() < r2.getIssuer().getNumVal()) {
					return 1;
				}
				if (r1.getIssuer().getNumVal() > r2.getIssuer().getNumVal()) {
					return -1;
				}
				// they are equal
				if (r1.getRuleType().getNumVal() < r2.getRuleType().getNumVal()) {
					return 1;
				}
				if (r1.getRuleType().getNumVal() > r2.getRuleType().getNumVal()) {
					return -1;
				}
				return 0;

			}

		});

		return OrderedRuleList;
	}

	static public String eleToString(Element el) {

		Document doc = new Document(el);
		return doc.toXML();
	}

	static public String eleToStringPretty(Element el) {

		Document doc = new Document(el);
		try {
			// Pretty print
			Serializer serializer = new Serializer(System.out, "UTF-8");
			serializer.setIndent(4);
			serializer.write(doc);
			serializer.flush();

			return doc.toXML();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "asd";
	}

	public static void setApplicationDomain(String appDomainString) {
		appDomain=appDomainString;
		
	}
	public static String getApplicationDomain() {

		if(appDomain.length()>1)
		return ":"+appDomain;
		else
			return "";
	}
}
