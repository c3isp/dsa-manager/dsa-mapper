package it.cnr.iit.c3isp.dsamapper.restapi.types;

public class RequestType {

    String id;
    String dsa;

    public RequestType() {
        this.id = "Fail To get Request or no Request argument at all.";
        this.dsa = null;
    }

    public RequestType(String id, String dsa) {
        this.id = id;
        this.dsa = dsa;
    }

    public String getId() {
        return this.id;
    }

    public String getDsa() {
        return this.dsa;
    }
    public void setId(String id){
    	
    	this.id=id;
    }
    public void setDsa(String dsa){
    	this.dsa=dsa;
    }
}
