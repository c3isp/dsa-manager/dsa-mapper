package it.cnr.iit.c3isp.dsamapper.mapper.rulehandler;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.wso2.balana.utils.Constants.PolicyConstants;
import org.wso2.balana.utils.Constants.PolicyConstants.XACMLData;
import org.wso2.balana.utils.policy.dto.AllOfElementDTO;
import org.wso2.balana.utils.policy.dto.AnyOfElementDTO;
import org.wso2.balana.utils.policy.dto.ApplyElementDTO;
import org.wso2.balana.utils.policy.dto.AttributeDesignatorDTO;
import org.wso2.balana.utils.policy.dto.AttributeValueElementDTO;
import org.wso2.balana.utils.policy.dto.ConditionElementDT0;
import org.wso2.balana.utils.policy.dto.MatchElementDTO;
import org.wso2.balana.utils.policy.dto.ObligationElementDTO;
import org.wso2.balana.utils.policy.dto.RuleElementDTO;
import org.wso2.balana.utils.policy.dto.TargetElementDTO;

import it.cnr.iit.c3isp.dsamapper.mapper.CommonUtils;
import it.cnr.iit.c3isp.dsamapper.mapper.DatumException;
import it.cnr.iit.c3isp.dsamapper.mapper.DatumManager;
import it.cnr.iit.c3isp.dsamapper.mapper.MapperException;
import it.cnr.iit.c3isp.dsamapper.mapper.UPOLBuilder;
import it.cnr.iit.c3isp.dsamapper.mapper.configjson.AttributeJson;
import it.cnr.iit.c3isp.dsamapper.mapper.configjson.ConfigJsonManager;
import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.AbstractRule.RuleType;
import java.io.UnsupportedEncodingException;

public abstract class AbstractRule {
	final static Logger log = LogManager.getLogger();

	public enum Issuer {
		USER(3), LEGAL_EXPERT(2), POLICY_EXPERT(1);
		private int numVal;

		Issuer(int num) {
			this.numVal = num;
		}

		public int getNumVal() {
			return this.numVal;
		}
	}

	public enum RuleType {
		AUTH(2), OBLIG(1), PROIBH(3);
		private int numVal;

		RuleType(int num) {
			this.numVal = num;
		}

		public int getNumVal() {
			return this.numVal;
		}

	}

	public enum InternalState {
		CREATED, VALID_MAPPED, TERMSSCOLLAPSED, NOT_VALID_SKIP
	}

	protected RuleType ruleType;

	public RuleType getRuleType() {
		return ruleType;
	}

	protected Element original_dom_parent_element = null; // reference to
															// original dom

	// element, can be changed
	// directly cause it's a
	// pointer <authorization id="AUTHORIZATION_0" index="0" isProtected="false"
	// conflict="false">

	public Element getOriginal_dom_parent_element() {
		return original_dom_parent_element;
	}

	protected String original_cnl_rule_string = null; // the content of dom of
														// tag cnl
	protected Element original_cnl_rule_dom = null; // the cnl dom element

	protected String collapsedTerms_rule_string = null; // the first pass
														// collpased, just the
														// Terms

	protected RuleElementDTO finalMappedRule = null; // the final rule mapped.

	protected String dataClassification = null;
	protected String purpose = null;

	protected String NameOftheRule = null;

	public String getNameOftheRule() {
		return NameOftheRule;
	}

	protected InternalState internalState = InternalState.CREATED;
	protected Issuer issuer;

	public Issuer getIssuer() {
		return issuer;
	}

	protected boolean isDerived=false;
	public boolean isDerived() {
		return isDerived;
	}
	public void setDerived(boolean derived) {
		this.isDerived=derived;
	}
	protected boolean isProtected;

	public boolean isProtected() {
		return isProtected;
	}

	protected boolean isContinousRule = false; // at least one term it's

	public boolean isContinousRule() {
		return isContinousRule;
	}

	// continuous
	protected List<TermHandler> att_list = new ArrayList<>();

	abstract protected RuleElementDTO getXACMLMappedRule() throws RuleException, DatumException;

	abstract public Element getDomFinalRule() throws RuleException;

	protected boolean collapseTerms() throws RuleException, DatumException {

		log.debug("########################## New collapse begin ########");
		log.debug("origin string: " + original_cnl_rule_string);

		// spazio 1 o infinite volte, (not 1 volta,spazio 1 o inf volte, tutto
		// questo zero o una volta), minimo 3 caratteri, poi una tonda, poi
		// qualsiasi cosa minimo 5 massimo 15 caratteri,chiusa tonda

		Pattern pattern = Pattern
				.compile("(\\s)+((not){1}(\\s)+)?(\\w){3,}\\((.){5,15}\\)");

		Matcher matcher = pattern.matcher(original_cnl_rule_string);

		collapsedTerms_rule_string = original_cnl_rule_string;
		int countAt = 1;

		while (matcher.find()) {
			String codeName = "@" + countAt; // code name for thi term @1,@2 am
												// not even sure to do this
												// here, but i'm the collapser
												// who should do this ?

			String trimmed = matcher.group().trim();
			collapsedTerms_rule_string = collapsedTerms_rule_string.replace(
					trimmed, codeName);

			boolean notted = StringUtils.startsWith(trimmed, "not ");
			if (notted) {
				trimmed = trimmed.substring(4); // remove "not ", but can be
												// also other whitespace after
												// not so
				trimmed = trimmed.trim(); // remove anything left
			}
			String attribute_label = trimmed.substring(0, trimmed.indexOf("("));
			int s1 = trimmed.indexOf("(?");
			int s2 = trimmed.indexOf(",");
			String Xchi = trimmed.substring(s1 + 2, s2);
			s1 = trimmed.indexOf(",?");
			s2 = trimmed.indexOf(")");
			String Xcosa = trimmed.substring(s1 + 2, s2);
                        String chi_resolved = DatumManager.resolveXDatum(Xchi);
                        String cosa_resolved = DatumManager.resolveXDatum(Xcosa);
                        
			AttributeJson at = ConfigJsonManager
					.getAttributeConfigFromCnlLabel(attribute_label);
			if (at == null || chi_resolved == null || chi_resolved.isEmpty()
					|| cosa_resolved == null || cosa_resolved.isEmpty()) {

				internalState = InternalState.NOT_VALID_SKIP;

				throw new RuleException(
						"Something unrecognized in rule. attribute: "
								+ attribute_label
								+ " Recognized attribute in cfg: "
								+ ((at == null) ? " ##NO!##" : "Yes")
								+ " chi: " + Xchi + " cosa: " + Xcosa
								+ " chi_resolved: " + chi_resolved
								+ " cosa_resolved: " + cosa_resolved);
			}
			String OpenAtributeValue = "";

			this.isContinousRule = at.CONT;

			if (at.OPENFIELD == true) {
                            // se il termine e' a campo aperto.......
                            
                            OpenAtributeValue = DatumManager.getValueForDatum(Xcosa);
                            // questo perche  e'  il  TERMINE  ad essere considerato
                            // aperto  ed e' sempre il secondo parametro a dover
                            //  avere il value
                            if (OpenAtributeValue == null || OpenAtributeValue.isEmpty()) {
                                    internalState = InternalState.NOT_VALID_SKIP;

                                    throw new RuleException(
                                                    "Could not find value in datum for an openfield term: "
                                                                    + attribute_label
                                                                    + " in the second datum: " + Xcosa
                                                                    + " resolved as: " + cosa_resolved);

                            }

                            log.debug("Found open attribute and his value: "
                                            + attribute_label + " datum: " + Xcosa + " val: "
                                            + OpenAtributeValue);
			}

			/*
			 * public GhostContainer(String codeName, String cnlLabel, String
			 * chi_x, String cosa_x, String chi_resolved, String cosa_resolved,
			 * boolean notted, String categ, String id, boolean open, boolean
			 * continuous, String openfieldvaluep) {
			 */
                        try {
                            TermHandler ThisTerm = new TermHandler(codeName, attribute_label,
                                            Xchi, Xcosa, chi_resolved, cosa_resolved, notted, at.CAT,
                                            at.ID, at.OPENFIELD, at.CONT, OpenAtributeValue);

                            att_list.add(ThisTerm);
                        } catch(Exception e) {
                            throw new RuleException("Error creating term handler: " + e.getMessage());
                        }

			countAt++;

		}

		log.debug("Collapsed rule: " + collapsedTerms_rule_string);
		log.debug("########################## collapse done success########");

		internalState = InternalState.TERMSSCOLLAPSED;
		
		if (countAt == 1) {

			log.debug("No term was found. Maybe a rule with only a 'can' referring to the system? Lets try keep going with this");
			//throw new RuleException("No one well formed Term was found in the rule");
			return false;
		}


		return true;

	}

	protected TargetElementDTO createXACMLTargetActionPart(String action)
			throws RuleException {

		TargetElementDTO target = new TargetElementDTO();

		AnyOfElementDTO any = new AnyOfElementDTO();
		AllOfElementDTO all = new AllOfElementDTO();
		MatchElementDTO match = new MatchElementDTO();

		match.setMatchId("urn:oasis:names:tc:xacml:1.0:function:string-equal");

		AttributeValueElementDTO attValue = new AttributeValueElementDTO();
		attValue.setAttributeDataType("http://www.w3.org/2001/XMLSchema#string");
		attValue.setAttributeValue(action);

		AttributeDesignatorDTO attDesignator = new AttributeDesignatorDTO();
		attDesignator.setCategory(PolicyConstants.ACTION_CATEGORY_URI);
		attDesignator.setAttributeId(PolicyConstants.ACTION_ID+UPOLBuilder.getApplicationDomain());
		attDesignator.setDataType("http://www.w3.org/2001/XMLSchema#string");
		attDesignator.setMustBePresent("true"); // Indeterminate instaed of
												// empty bag
		match.setAttributeDesignatorDTO(attDesignator);
		match.setAttributeValueElementDTO(attValue);

		all.getMatchElementDTOs().add(match);
		any.getAllOfElementDTOs().add(all);
		target.getAnyOfElementDTOs().add(any);

		return target;

	}

	protected ConditionElementDT0 createXACMLConditionPart(boolean collapsedpresent) throws RuleException {

		ConditionElementDT0 condition = new ConditionElementDT0();

		if(collapsedpresent) {
		String logicalPart = getCollapsedLogicPart(this.collapsedTerms_rule_string);
		ApplyElementDTO applyLogicPreSurrounding = recursiveConvertXacmlLogicPart(logicalPart);
		ApplyElementDTO applylogicAfterSurround = createFinalApplyforConditionSurroundbyStuffs(applyLogicPreSurrounding);

		condition.setApplyElement(applylogicAfterSurround);
		}
		else {	
			ApplyElementDTO applylogicAfterSurround = createFinalApplyforConditionSurroundbyStuffs(null);
			condition.setApplyElement(applylogicAfterSurround);
		}
		return condition;

	}

	protected ApplyElementDTO createFinalApplyforConditionSurroundbyStuffs(
			ApplyElementDTO applyConditionBeforeStuffs) {

		ApplyElementDTO applyAvailable = CommonUtils
				.createApplyForStringEqualTest(
						"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
						"urn:oasis:names:tc:xacml:3.0:resource:dsa-status",
						"AVAILABLE");

		ApplyElementDTO applyPurpose = CommonUtils
				.createApplyForStringEqualTest(
						"urn:oasis:names:tc:xacml:1.0:subject-category:access-subject",
						"urn:oasis:names:tc:xacml:3.0:subject:access-purpose",
						this.purpose);

		ApplyElementDTO applyclassificationCategory = CommonUtils
				.createApplyForStringEqualTest(
						"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
						"urn:oasis:names:tc:xacml:3.0:resource:resource-classification",
						this.dataClassification);

		List<ApplyElementDTO> listAND = new ArrayList<ApplyElementDTO>();
		if (applyConditionBeforeStuffs != null) {
			listAND.add(applyConditionBeforeStuffs);

		}
		listAND.add(applyAvailable);
		//listAND.add(applyPurpose);
		//listAND.add(applyclassificationCategory);

		return CommonUtils.createANDApply(listAND);
	}

	private String getNameOftheRule(Element domRule) throws RuleException {

		String name = domRule.getAttribute("id");
		if (name == null || name.isEmpty()) {
			throw new RuleException("Could not get name of rule: "
					+ domRule.getTextContent());
		}

		log.debug("Rule Name: " + name);
		return name;
	}

	protected void initCnlFromDomElement() throws RuleException {
		log.debug("init abstract rule structure..");
                
		// name will used in creating of xacml rule
                this.NameOftheRule = getNameOftheRule(original_dom_parent_element); 
                

		original_cnl_rule_dom = CommonUtils
				.FindDeep_FirstChildElementByTagAndAttribute(
						original_dom_parent_element, "expression", "language",
						"CNL4DSA");
		if (original_cnl_rule_dom == null) {
			log.error("Cln for auth not found.Giving up. Original_dom_elem full text: "
					+ original_dom_parent_element.getTextContent());
			throw new RuleException("Cln for auth not found.Giving up.");
		}

		// Let's clean a little the string
		String oString = original_cnl_rule_dom.getTextContent();
		oString = oString.replaceAll("\\s+", " ");
		oString = oString.trim();
		/*
		 * 16/10/2018
		 * Qui bisogna aggiustare il discorso che vogliono infilare la dmo anche nella regola. Un esempio:
		 *       <expression language="CNL4DSA" issuer="Policy Expert">
		 *       if  hasRole(?X_26,?X_27)  then  after [?X_28, AnonymizeDelimitedStringsBySuppression, ?X_29]  then  can [?X_26, Create, ?X_29]</expression>
	
		Quindi la soluzione piu semplice e' cercare then after e la quadra chiusa, finche' si trova eliminare
		
		
		*/
		if(!oString.contains("must")) {
		String strToFind="then after";
		while(oString.contains(strToFind)) {
			log.debug("\"After\" in original string in AuthProb.");
			log.debug("Before cut oString: "+oString);

			int startIndex=oString.indexOf(strToFind);
			int endIndex=oString.indexOf("]", startIndex);
			String toBeReplaced=oString.substring(startIndex, endIndex+1);
			oString=oString.replace(toBeReplaced, "");
			log.debug("After cut oString: "+oString);
			
		}
		}
		
		//rifaccio il trim per essere sicuri con questi tagli non siano nati spazi aggiuntivi,
		//e' un po esagerato ma costa poco
		
		oString = oString.replaceAll("\\s+", " ");
		oString = oString.trim();
		original_cnl_rule_string = oString;

		// //boolean valid = StringUtils
		// .contains(original_cnl_rule_string, " then ");

		String strisProtected = original_dom_parent_element
				.getAttribute("isProtected");
		if (strisProtected == null || strisProtected.isEmpty()) {
			throw new RuleException("There isn't any kind of isProtected:"
					+ original_cnl_rule_string);

		}
		if (strisProtected.equalsIgnoreCase("true")) {
			this.isProtected = true;
		} else if (strisProtected.equalsIgnoreCase("false")) {
			this.isProtected = false;

		} else {
			throw new RuleException("can't understand isProtected of rule :"
					+ original_cnl_rule_string);
		}

		String strisIssuer = original_cnl_rule_dom.getAttribute("issuer");
		if (strisIssuer == null || strisIssuer.isEmpty()) {
			throw new RuleException("There isn't any kind of issuer:"
					+ original_cnl_rule_string);

		}
		if (strisIssuer.equalsIgnoreCase("User")) {
			this.issuer = Issuer.USER;
		} else if (strisIssuer.equalsIgnoreCase("Legal Expert")) {
			this.issuer = Issuer.LEGAL_EXPERT;

		} else if (strisIssuer.equalsIgnoreCase("Policy Expert")) {
			this.issuer = Issuer.POLICY_EXPERT;

		} else {
			throw new RuleException("can't understand issuer of rule :"
					+ original_cnl_rule_string);
		}

		/*
		 * if (valid) { log.debug("valid rule for now.."); return; } log.error(
		 * "Found cln rule string, but it's not valid. There isn't any * then *, aborting.."
		 * ); throw new RuleException("Not well formed cln rule");
		 */
		return;

	}

	private ApplyElementDTO recursiveConvertXacmlLogicPart(
			String collapsedlogicapart) throws RuleException {

		/*
		 * 
		 * if string=@n return converted apply
		 */
		collapsedlogicapart = collapsedlogicapart.trim();
		if (collapsedlogicapart.startsWith("@")
				&& (collapsedlogicapart.length() < 5)) {
			return getApplyTerm(collapsedlogicapart); // this is just a term to
														// convert
		}

		if (isThisJustanOR(collapsedlogicapart)) {
			return constructOr(collapsedlogicapart);

		}

		/*
		 * Trova qualsiasi cosa in maniera non greedy fino a che non trovi uno
		 * spazio and spazio oppure la fine della frase ma questo tutto, deve
		 * essere preceduto o dall'inizio della frase oppure da uno spazio and
		 * spazio
		 * 
		 * ((?<=^)|(?<=and\\s)).+?(?:(?=\\sand\\s|$))
		 */
		//
		collapsedlogicapart = collapsedlogicapart.replaceAll(" AND ", " and ");

		String exp = "((?<=^)|(?<=and\\s)).+?(?:(?=\\sand\\s|$))";
		Pattern pattern = Pattern.compile(exp);

		Matcher matcher = pattern.matcher(collapsedlogicapart);

		int count = 0;
		ApplyElementDTO ApplyAND = new ApplyElementDTO();

		ApplyAND.setFunctionId(XACMLData.FUNCTION_AND);

		while (matcher.find()) {

			String thisAndoperand = matcher.group().trim();

			ApplyAND.getApplyElements().add(
					recursiveConvertXacmlLogicPart(thisAndoperand));

			count++;
		}

		if (count < 2) {
			throw new RuleException(
					"Looklike this was the entire complex rule in recursion,"
							+ " but i can't find any suitable operand for AND. "
							+ collapsedlogicapart);
		}
		return ApplyAND;
	}

	private ApplyElementDTO constructOr(String orstring) throws RuleException {

		Pattern pattern = Pattern.compile("@(\\d){1,3}");

		Matcher matcher = pattern.matcher(orstring);

		ApplyElementDTO ApplyOR = new ApplyElementDTO();
		// Can't find OR in xacml constant or policyconstant -_________________-

		ApplyOR.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:or");

		int count = 0;
		// String finalOR = "";
		while (matcher.find()) {

			String thisTermSymbol = matcher.group().trim();

			ApplyOR.getApplyElements().add(getApplyTerm(thisTermSymbol));
			/*
			 * if (count == 0) {
			 * 
			 * finalOR = getApplyTerm(thisTermSymbol); } else { finalOR =
			 * finalOR + " OR " + getApplyTerm(thisTermSymbol); }
			 */
			count++;
		}
		if (count == 0)
			throw new RuleException("constructOr can't find any symbol @n");

		// return finalOR;
		return ApplyOR;
	}

	private boolean isThisJustanOR(String rule) throws RuleException {

		rule = rule.replaceAll(" OR ", " or ");
		// inizio stringa,chiocciola,1 fino a 3 numeri, un numero da 1 a inf di
		// ( spazio, or, spazio, chiocciola con numero), fine stringa
		// ^@(\d){1,3}((\s)or\s@(\d){1,3}){1,}$
		Pattern pattern = Pattern
				.compile("^@(\\d){1,3}((\\s)or\\s@(\\d){1,3}){1,}$");

		Matcher matcher = pattern.matcher(rule);

		int count = 0;

		while (matcher.find()) {

			// String trimmed = matcher.group().trim();
			count++;
		}

		if (count == 1)
			return true; // just 1 match, it's perfect this is an OR
		if (count == 0)
			return false;// no match at all, this is not an OR
		throw new RuleException("Something really bad during OR check. rule: "
				+ rule);
	}

	private ApplyElementDTO getApplyTerm(String simbol) throws RuleException {

		for (TermHandler at : this.att_list) {
			if (at.getCodeName().equals(simbol)) {
				return at.getApply_XACML();
			}
		}
		throw new RuleException("Can't find symbol in list: " + simbol);
	}

	private String getCollapsedLogicPart(String rule) throws RuleException {

		int ifpos = rule.indexOf("if ");
		if (ifpos < 0)
			throw new RuleException("can't find IF in rule: " + rule);
		int thenpos = rule.indexOf(" then ");
		if (thenpos < 0)
			throw new RuleException("can't find THEN in rule: " + rule);
		String logic = rule.substring(ifpos + 3, thenpos);
		logic = logic.replaceAll("\\s+", " ");
		logic = logic.replaceAll("\\(", "");
		logic = logic.replaceAll("\\)", "");
		return logic.trim();

	}

	/*
	 * Snicky Snicky
	 * 
	 * Questo funziona anche con le obligation, grazie al comportamento della
	 * indexOf. Cioe' tornando la prima quadra che trova, si trovera' solo il
	 * "then after", cioe' la tripletta di [x,READ/Write,x]. E quindi si puo'
	 * usare questo nella classe astratta per creare il target della regola
	 * 
	 * Nelle obligation ci sara' un'atra funzione specifica per trovare la
	 * seconda tripletta dove ci sara' per ora un po' sempre scritto
	 * [system,log,event]
	 */
	protected String getMatchActionFromActionPart(String tripleteAction)
			throws RuleException {

		if (tripleteAction.toLowerCase().contains("read")) {
			return "read";
		} 
	    if (tripleteAction.toLowerCase().contains("write")) {
			return "write";
		}
	    if (tripleteAction.toLowerCase().contains("copy")) {
			return "copy";
		}
	    if (tripleteAction.toLowerCase().contains("delete")) {
			return "delete";
		}
		
		int i1 = tripleteAction.indexOf(",");
		int i2 = tripleteAction.indexOf(",", i1 + 1);

		String action = tripleteAction.substring(i1 + 1, i2);
		action = action.trim();

		if (action.equalsIgnoreCase("read")) {
			return "read";
		} else if (action.equalsIgnoreCase("write")) {
			return "write";
		} else if (action.equalsIgnoreCase("copy")) {
			return "copy";
		} else if (action.equalsIgnoreCase("delete")) {
			return "delete";
		} else
			log.warn("Unrecognized action. Return as it is, lowered.");
			return action.toLowerCase();

	}

	protected String getCollapsedActionPart(String rule) throws RuleException {

		int br1 = rule.indexOf("[");
		if (br1 < 0)
			throw new RuleException("can't find [ in rule: " + rule);
		int br2 = rule.indexOf("]");
		if (br2 < 0)
			throw new RuleException("can't find ] in rule: " + rule);
		String actionpart = rule.substring(br1, br2 + 1);
		actionpart = actionpart.replaceAll("\\s+", " ");

		return actionpart.trim();
	}
	
protected List<ObligationElementDTO> getObligationStatmentinfo(String statmentInfoContent) throws RuleException, UnsupportedEncodingException {
		
		ObligationElementDTO ob=new ObligationElementDTO();
		String encoded= URLEncoder.encode(statmentInfoContent, "UTF-8");
		ob.setId(encoded);
		ob.setType(ObligationElementDTO.OBLIGATION);
		ob.setEffect((this.ruleType == RuleType.AUTH) ? "Permit" : "Deny");
		ArrayList<ObligationElementDTO> a=new ArrayList<>();
		a.add(ob);
		
		return a;
		
		
	}

}
