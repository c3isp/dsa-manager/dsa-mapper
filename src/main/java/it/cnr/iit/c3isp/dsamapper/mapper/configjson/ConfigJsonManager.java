/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper.configjson;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

/**
 *
 * @author root
 */
public class ConfigJsonManager {

	private static JsonConfig jsonconfig = null;
	static final ClassLoader loader = ConfigJsonManager.class.getClassLoader();

	public static void BootstrapConfig() throws Exception {
		// File lol=ResourceUtils.getFile("config.json");
		// InputStream is
		// =AttributeConfigJsonManager.class.getResourceAsStream("config.json");
		// InputStream is=new FileInputStream(lol);
		InputStream is = loader.getResourceAsStream("config.json");
		if (is == null) {
			throw new Exception("Could not find config json.");

		}
		String StringFromInputStream = IOUtils.toString(is, "UTF-8");
		Gson gb = new Gson();
		jsonconfig = gb.fromJson(StringFromInputStream, JsonConfig.class);

	}

	public static JsonConfig getCFG() throws Exception {
		if (jsonconfig == null) {
			BootstrapConfig();
		}
		return jsonconfig;
	}

	public static AttributeJson getAttributeConfigFromCnlLabel(String label) {

		AttributeJson att = null;
		for (AttributeJson at : jsonconfig.ATTRS_CFG) {

			if ((at.ATTR).equalsIgnoreCase(label)) {
				att = at;
				break;
			}
		}

		/*
		 * if (att == null) { throw new
		 * Exception("Attribute in config with label: " + label +
		 * " not found."); }
		 */
		return att;
	}

	public static ObligationJson getMappedObligatio(String cnlobli)
			throws Exception {

		ObligationJson att = null;
		for (ObligationJson at : jsonconfig.OBLIGATIONS_SETUP) {

			if ((at.DSA_OB).equalsIgnoreCase(cnlobli)) {
				att = at;
				break;
			}
		}

		/*
		 * if (att == null) { throw new
		 * Exception("Obligation in config with label: " + cnlobli +
		 * " not found."); }
		 */
		return att;
	}
}
