/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author root
 */
public class DatumManager {

	private static Map<String, String> DatumMap = new HashMap<>();
	private static Map<String, String> ValueMap = new HashMap<>();
	final static Logger log = LogManager.getLogger();

	// private static Boolean ready=false;
	public static void MapThisDSADatum(Node NodeData) throws DatumException {
		DatumMap.clear();
		List<Element> expressionlist = CommonUtils
				.FindDeep_AllChildsByTagAndAttribute(NodeData, "expression",
						"language", "CNL4DSA");

		for (Element el : expressionlist) {
			// System.out.println(el.getTextContent());
			String text = el.getTextContent();
			int questionmark = text.indexOf("?");
			int endVarNam = text.indexOf(" is-a");
			String varnam = text.substring(questionmark + 1, endVarNam);
			int s1 = text.indexOf("#");
			int s2 = text.indexOf(">");
			String label = text.substring(s1 + 1, s2);
			// System.out.println(varnam+" - "+label);
			if (questionmark == -1 || endVarNam == -1 || s1 == -1 || s2 == -1) {
				log.error("I can't understand this expression: " + text);
			}
			DatumMap.put(varnam, label);
			log.debug("Datum varname:" + varnam + " label:" + label);

			// ok label is now stored, let's store also the value (if any) of
			// this datum
			/*
			 * <datum value="1234,5678,10" id="DATUM_X_7"> <expression
			 * language="CNL4DSA">?X_7 is-a
			 * &lt;http://testcocodsa.iit.cnr.it:8080
			 * /vocabularies/healthcare_vocabulary.owl#Area&gt;</expression>
			 * </datum>
			 */

			Element parentDatum = (Element) el.getParentNode(); // we have
																// expression,
																// let's get
																// datum
			String valueOfDatum = parentDatum.getAttribute("value");
			if (valueOfDatum != null && !valueOfDatum.isEmpty()) {
				log.debug("Found value for: " + varnam + " as " + valueOfDatum);
				ValueMap.put(varnam, valueOfDatum);
			}
		}

	}

	// public static Boolean isReady(){return ready;}

	public static String resolveXDatum(String Xname) throws DatumException {

		String val = DatumMap.get(Xname);
		if (val == null) {
			throw new DatumException("Datum not found.");
		}
		return val;

	}

	public static String getValueForDatum(String Xname) throws DatumException {

		String val = ValueMap.get(Xname);
		if (val == null || val.isEmpty()) {
			throw new DatumException("There isn't any value for this datum. Xname="
					+ Xname);
		}
		return val;

	}
}
