package it.cnr.iit.c3isp.dsamapper.mapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class ConfigManagerv2 {
    @Value("${rest.endpoint.dsaapi}")
    private String dsaapi_endpoint;
    
    public String getDsaapi_endpoint() {
        return dsaapi_endpoint;
    }
    
    public void setDsaapi_endpoint(String setting1) {
        this.dsaapi_endpoint = setting1;
    }
}
