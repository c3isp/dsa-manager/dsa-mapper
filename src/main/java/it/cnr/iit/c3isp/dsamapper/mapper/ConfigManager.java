package it.cnr.iit.c3isp.dsamapper.mapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigManager {
	
	@Value("${rest.endpoint.dsaapi}")
	private String dsaapi_endpoint;
	
	private static ConfigManager instance=null;
	private ConfigManager() {}
	public static ConfigManager getInstance() {
		
		if(instance==null) {
			instance=new ConfigManager();
		}
		return instance;
	}
	public String getDsaApiEndpoint() throws Exception {
		if(dsaapi_endpoint==null || dsaapi_endpoint.isEmpty()) {
			throw new Exception("ConfigManager : dsaapi_endpoint not valid.");
		}
		return dsaapi_endpoint;
	}

}
