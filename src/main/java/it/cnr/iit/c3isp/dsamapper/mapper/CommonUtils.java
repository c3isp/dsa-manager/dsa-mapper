/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wso2.balana.utils.PolicyUtils;
import org.wso2.balana.utils.Constants.PolicyConstants;
import org.wso2.balana.utils.Constants.PolicyConstants.XACMLData;
import org.wso2.balana.utils.exception.PolicyBuilderException;
import org.wso2.balana.utils.policy.dto.ApplyElementDTO;
import org.wso2.balana.utils.policy.dto.AttributeDesignatorDTO;
import org.wso2.balana.utils.policy.dto.AttributeValueElementDTO;
import org.wso2.balana.utils.policy.dto.ConditionElementDT0;
import org.wso2.balana.utils.policy.dto.RuleElementDTO;
import org.xml.sax.SAXException;

import it.cnr.iit.c3isp.dsamapper.mapper.rulehandler.AbstractRule;
import org.w3c.dom.DOMException;

/**
 *
 * @author root
 */
public class CommonUtils {
	final static Logger log = LogManager.getLogger();

	public static ApplyElementDTO createApplyForStringEqualTest(
			String xacmlDesignatorCategory, String xacmlDesignatorID,
			String xacmlAttValue) {
		
	
		
		ApplyElementDTO ApplyStringEqual = new ApplyElementDTO();
		ApplyStringEqual
				.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:string-equal");

		AttributeValueElementDTO attValue = new AttributeValueElementDTO();
		attValue.setAttributeDataType("http://www.w3.org/2001/XMLSchema#string");
		attValue.setAttributeValue(xacmlAttValue);

		ApplyElementDTO stringOneAndOnlyApply = createStringOneAndOnlyWithDesignatorApply(
				xacmlDesignatorCategory, xacmlDesignatorID);
		ApplyStringEqual.setApplyElement(stringOneAndOnlyApply);

		ApplyStringEqual.setAttributeValueElementDTO(attValue);
		// log.debug("xacml created string is equal test /w designator & value");

		return ApplyStringEqual;
	}

	public static ApplyElementDTO notThisApply(ApplyElementDTO elementToNot) {
		log.debug("applying not..");
		ApplyElementDTO notApply = new ApplyElementDTO();

		notApply.setFunctionId(PolicyConstants.XACMLData.FUNCTION_NOT);
		notApply.setApplyElement(elementToNot);
		return notApply;
	}

	public static AttributeDesignatorDTO createDesignator(String cat,
			String id, String type) {
		//05/12/2018 modifica fatta per applicare l'application domain, e' piu semplice fare questa mod su commonUtils perche'
		// quando si ha a che fare con gli ID degli attributi alla fine sia che provengano dal config, sia che siano hardcoded
		//si passa sempre di qui per la creazione dell xacml
		id=id+UPOLBuilder.getApplicationDomain();
		
		AttributeDesignatorDTO des = new AttributeDesignatorDTO();
		des.setCategory(cat);
		des.setAttributeId(id);
		des.setDataType(type);
		des.setMustBePresent("true");

		return des;
	}

	public static ApplyElementDTO createStringOneAndOnlyWithDesignatorApply(
			String xacml_category, String xacml_id) {
		//05/12/2018 modifica fatta per applicare l'application domain, e' piu semplice fare questa mod su commonUtils perche'
		// quando si ha a che fare con gli ID degli attributi alla fine sia che provengano dal config, sia che siano hardcoded
		//si passa sempre di qui per la creazione dell xacml
		xacml_id=xacml_id+UPOLBuilder.getApplicationDomain();
		
		
		ApplyElementDTO stringOne = new ApplyElementDTO();
		stringOne
				.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:string-one-and-only");

		AttributeDesignatorDTO attDesignator = new AttributeDesignatorDTO();
		attDesignator.setCategory(xacml_category);
		attDesignator.setAttributeId(xacml_id);
		attDesignator.setDataType("http://www.w3.org/2001/XMLSchema#string");
		attDesignator.setMustBePresent("true"); // Indeterminate instaed of
												// empty bag
		stringOne.setAttributeDesignators(attDesignator);
		// log.debug("xacml created string-one-and-only /w designator..");

		return stringOne;
	}

	public static void removeAllChilds(Node node) {
		while (node.hasChildNodes())
			node.removeChild(node.getFirstChild());
	}

	public static void DebugSingleElement(Element el) {
		System.out.println("El Tagname=" + el.getTagName());
		System.out.println("El textcontent=" + el.getTextContent());
		System.out.println("El nodeval=" + el.getNodeValue());

		NamedNodeMap nl = el.getAttributes();

		for (int i = 0; i < nl.getLength(); ++i) {
			Node attr = nl.item(i);
			System.out.println(attr.getNodeName() + " = \""
					+ attr.getNodeValue() + "\"");
		}

	}

	public static Document getDSADoc(String strDSA)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream streamDSA = new ByteArrayInputStream(
				strDSA.getBytes(StandardCharsets.UTF_8));

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(streamDSA);
		return doc;
	}

	public static List<Element> FindDeep_AllChildsByTagAndAttribute(
			Node parent_node, String Tag, String AttributeName,
			String AttributeValue) {

		List<Element> list = new ArrayList<>();
		RecursiveAddChilsTagAndAttribute(list, parent_node, Tag, AttributeName,
				AttributeValue);
		return list;

	}

	private static void RecursiveAddChilsTagAndAttribute(List<Element> list,
			Node parent_node, String Tag, String AttributeName,
			String AttributeValue) {

		// get all child nodes
		NodeList nodes = parent_node.getChildNodes();

		// print the text content of each child
		int k = nodes.getLength();
		for (int i = 0; i < nodes.getLength(); i++) {

			Node thisnode = nodes.item(i);
			if (thisnode.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			String name = thisnode.getNodeName();
			if (thisnode.getNodeName().equalsIgnoreCase(Tag)) {
				if (thisnode instanceof Element) {
					Element el = (Element) thisnode;
					boolean ispresent = el.hasAttribute(AttributeName);
					String attvalue = el.getAttribute(AttributeName);
					if (ispresent && attvalue.equals(AttributeValue)) {
						list.add(el);
					}
				}
			}
			RecursiveAddChilsTagAndAttribute(list, thisnode, Tag,
					AttributeName, AttributeValue);
		}

	}

	public static ApplyElementDTO createANDApply(List<ApplyElementDTO> list) {

		ApplyElementDTO Applyand = new ApplyElementDTO();
		Applyand.setFunctionId(XACMLData.FUNCTION_AND);

		for (ApplyElementDTO at : list) {
			Applyand.getApplyElements().add(at);
		}
		return Applyand;
	}

	public static Node findFirstLevelChildByTagName(Node node, String tag) {

		NodeList nodes = node.getChildNodes();

		// print the text content of each child
		for (int i = 0; i < nodes.getLength(); i++) {

			Node thisnode = nodes.item(i);
			if (thisnode.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (thisnode.getNodeName().equalsIgnoreCase(tag)) {
				return thisnode;
			}
		}

		return null;

	}

	public static Element FindDeep_FirstChildElementByTagAndAttribute(
			Node node, String Tag, String AttributeName, String AttributeValue) {

		// get all child nodes
		NodeList nodes = node.getChildNodes();

		// print the text content of each child
		for (int i = 0; i < nodes.getLength(); i++) {

			Node thisnode = nodes.item(i);
			if (thisnode.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (thisnode.getNodeName().equalsIgnoreCase(Tag)) {
				if (thisnode instanceof Element) {
					Element el = (Element) thisnode;
					if (AttributeName != null && AttributeValue != null) {
						boolean ispresent = el.hasAttribute(AttributeName);
						String attvalue = el.getAttribute(AttributeName);
						if (ispresent && attvalue.equals(AttributeValue)) {
							return el;
						}
					} else {

						return el; // this is the element with tag and we don't
									// care about attribute name/value
					}
				}
			}
			Element el2 = FindDeep_FirstChildElementByTagAndAttribute(thisnode,
					Tag, AttributeName, AttributeValue);
			if (el2 != null) {
				return el2;
			}
		}

		return null;
	}

	public static String convertDomNodeToString(Node a)
			throws TransformerException {

		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(a);
		transformer.transform(source, result);

		String xmlString = result.getWriter().toString();
		return xmlString;
	}

	public static Element convertXacmlApplyElementToElement(
			ApplyElementDTO apply, Document doc)
			throws ParserConfigurationException, PolicyBuilderException,
			TransformerException {

		Element el = PolicyUtils.createApplyElement(apply, doc);
		return el;
		// log.debug(convertDomNodeToString(el));

	}

	public static Element convertXacmlRuleElementToElement(RuleElementDTO rule,
			Document doc) throws PolicyBuilderException {

		Element el = PolicyUtils.createRuleElement(rule, doc);
		return el;
		// log.debug(convertDomNodeToString(el));

	}

	public static void adjustDecisionTimeAndNsAndPatchPasteONGOING(
			Element standardRule, AbstractRule rule) throws DOMException {
		// TODO Auto-generated method stub
		Element cond = CommonUtils.FindDeep_FirstChildElementByTagAndAttribute(
				standardRule, "Condition", null, null);
		if (cond == null) {
			log.debug("No condition to adjust in rule.");
			return;
		}
		if (rule.isContinousRule()) {
			cond.setAttribute("DecisionTime", "ongoing");
		} else {
			cond.setAttribute("DecisionTime", "pre");
			// qui c'e' da replicare la condition mettendola in ongoing
			// patch pasta per accordare il comportamento aspettato a cosa
			// succede nel motore.

			Element copy = (Element) standardRule.getOwnerDocument()
					.importNode(cond, true);
			copy.setAttribute("DecisionTime", "ongoing");
			standardRule.appendChild(copy);

		}

	}

	public static Element getDoubleProibhDSAAVAILABLE(Document doc)
			throws MapperException {

		Element preProibh = CommonUtils.createProhibitionDSAAVAilable("pre",
				doc);
		Element ongoingProibh = CommonUtils.createProhibitionDSAAVAilable(
				"ongoing", doc);

		Element condOngoing = (Element) CommonUtils
				.findFirstLevelChildByTagName(ongoingProibh, "Condition");

		if (condOngoing == null) {
			throw new MapperException(
					"Could not find condition in ongoing proibh.Internal error.");
		}

		preProibh.appendChild(condOngoing);

		return preProibh;

	}

	private static Element createProhibitionDSAAVAilable(String decisionTime,
			Document doc) throws MapperException {

		RuleElementDTO mappedRule = new RuleElementDTO();

		mappedRule.setRuleEffect("Deny");
		mappedRule.setRuleId("PROHIBITION_DsaSanityCheck");

		ApplyElementDTO applyAvailable1 = CommonUtils
				.createApplyForStringEqualTest(
						"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
						"urn:oasis:names:tc:xacml:3.0:resource:dsa-status",
						"AVAILABLE");

		/*
		 * 
		 * ApplyElementDTO applytime = CommonUtils
		 * .createApplyTimeWithFunctionID(
		 * "urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal", ,
		 * , this.OpenFieldValue, this.NottedAttribute);
		 */
		String timegreaterFID = "urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal";
		String timelessFID = "urn:oasis:names:tc:xacml:1.0:function:time-less-than-or-equal";
		String designerEnvironment = "urn:oasis:names:tc:xacml:3.0:attribute-category:environment";
		String currTimeAttID = "urn:oasis:names:tc:xacml:3.0:environment:currentTime";

		
		
		ApplyElementDTO applyStartDate = CommonUtils
				.createApplyTimeWithFunctionID(timegreaterFID,
						designerEnvironment, currTimeAttID,
						UPOLBuilder.getValidity_StartDate(), false);

		ApplyElementDTO applyEndDate = CommonUtils
				.createApplyTimeWithFunctionID(timelessFID,
						designerEnvironment, currTimeAttID,
						UPOLBuilder.getValidity_EndDate(), false);

		ApplyElementDTO applyAvailable2 = CommonUtils
				.createApplyForStringEqualTest(
						"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
						"urn:oasis:names:tc:xacml:3.0:resource:dsa-version",
						MapperDirector.getDsaVersion());

		ApplyElementDTO applyAvailable3 = CommonUtils
				.createApplyForStringEqualTest(
						"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
						"urn:oasis:names:tc:xacml:3.0:resource:runtimeAttestation",
						"0");

		ApplyElementDTO applyAvailable3Not = CommonUtils
				.notThisApply(applyAvailable3);

		ApplyElementDTO andApply = new ApplyElementDTO();
		andApply.setFunctionId(PolicyConstants.XACMLData.FUNCTION_AND);
		andApply.getApplyElements().add(applyAvailable1);
		andApply.getApplyElements().add(applyAvailable2);
		
		
		//andApply.getApplyElements().add(applyAvailable3Not);   ////commentato perche' in c3isp non c'e' il runtimeattestation

		// andApply.getApplyElements().add(applyStartDate);
		// andApply.getApplyElements().add(applyEndDate);

		ApplyElementDTO nottedApply = CommonUtils.notThisApply(andApply);
                try {
                    Element ruleDom = convertXacmlRuleElementToElement(mappedRule, doc);

                    ConditionElementDT0 cond = new ConditionElementDT0();
                    cond.setApplyElement(nottedApply);

                    Element condEle = PolicyUtils.createConditionElement(cond, doc);

                    condEle.setAttribute("DecisionTime", decisionTime);

                    ruleDom.appendChild(condEle);

                    return ruleDom;
                } catch(PolicyBuilderException e) {
                    throw new MapperException("Error: " + e.getClass().toString() + "\n" + e.getMessage());
                }

	}

	public static ApplyElementDTO createApplyTimeWithFunctionID(
			String FunctionID, String designerCat, String designerID,
			String attvalue, boolean notted) {
		//05/12/2018 modifica fatta per applicare l'application domain, e' piu semplice fare questa mod su commonUtils perche'
		// quando si ha a che fare con gli ID degli attributi alla fine sia che provengano dal config, sia che siano hardcoded
		//si passa sempre di qui per la creazione dell xacml
		designerID=designerID+UPOLBuilder.getApplicationDomain();
		
		
		ApplyElementDTO applytime = new ApplyElementDTO();
		applytime.setFunctionId(FunctionID);

		ApplyElementDTO applyTimeone = new ApplyElementDTO();
		applyTimeone
				.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:time-one-and-only");

		AttributeDesignatorDTO attrDes = CommonUtils.createDesignator(
				designerCat, designerID,
				"http://www.w3.org/2001/XMLSchema#time");
		applyTimeone.setAttributeDesignators(attrDes);

		AttributeValueElementDTO attval = new AttributeValueElementDTO();
		attval.setAttributeDataType("http://www.w3.org/2001/XMLSchema#time");
		attval.setAttributeValue(attvalue);

		applytime.setApplyElement(applyTimeone);
		applytime.setAttributeValueElementDTO(attval);

		if (notted) {
			applytime = CommonUtils.notThisApply(applytime);
		}
		return applytime;
	}

	public static ApplyElementDTO createApplyDateWithFunctionID(String functionID, String category, String attributeID,
			String openFieldValue, boolean nottedAttribute) {
		attributeID=attributeID+UPOLBuilder.getApplicationDomain();
		
		
		ApplyElementDTO applytime = new ApplyElementDTO();
		applytime.setFunctionId(functionID);

		ApplyElementDTO applyTimeone = new ApplyElementDTO();
		applyTimeone
				.setFunctionId("urn:oasis:names:tc:xacml:1.0:function:date-one-and-only");

		AttributeDesignatorDTO attrDes = CommonUtils.createDesignator(
				category, attributeID,
				"http://www.w3.org/2001/XMLSchema#date");
		applyTimeone.setAttributeDesignators(attrDes);

		AttributeValueElementDTO attval = new AttributeValueElementDTO();
		attval.setAttributeDataType("http://www.w3.org/2001/XMLSchema#date");
		attval.setAttributeValue(openFieldValue);

		applytime.setApplyElement(applyTimeone);
		applytime.setAttributeValueElementDTO(attval);

		if (nottedAttribute) {
			applytime = CommonUtils.notThisApply(applytime);
		}
		return applytime;

	}
}
