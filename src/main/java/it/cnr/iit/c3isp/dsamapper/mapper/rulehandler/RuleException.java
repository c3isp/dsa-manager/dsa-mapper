/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.c3isp.dsamapper.mapper.rulehandler;

import it.cnr.iit.c3isp.dsamapper.mapper.MapperException;

/**
 *
 * @author Vincenzo Farruggia
 */
public class RuleException extends MapperException {
    public RuleException(String message) {
        super(message);
    }
}
