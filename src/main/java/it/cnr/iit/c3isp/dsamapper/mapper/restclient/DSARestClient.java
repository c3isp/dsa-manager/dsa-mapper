package it.cnr.iit.c3isp.dsamapper.mapper.restclient;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.client.RestClientException;


public class DSARestClient {
	private static final Logger log = LoggerFactory
			.getLogger(DSARestClient.class);


	public static String FetchDSAFromID(String ID,String endpoint) throws RestClientException {

		String uri = endpoint+"/getDSA/" + ID;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> las = (ResponseEntity) restTemplate
				.getForEntity(uri, String.class);
		HttpStatus status = las.getStatusCode();
		// System.out.println(status.value());
		if (!status.is2xxSuccessful()) {

			return null;
		}

		return las.getBody();
	}

	public static boolean UploadMappedDSA2(String dsa, String ID,String endpoint)
			throws Exception {
		String uri = endpoint+"/updatedsa";
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add("dsaid", ID);

		/*
		 * String filePath =
		 * DsaStorage.getInstance().getTempFilePath(dsaContent,dsaID);
		 * parts.add("file", new FileSystemResource(new File(filePath)));
		 */
		InputStream stream = new ByteArrayInputStream(
				dsa.getBytes(StandardCharsets.UTF_8));
		parts.add("file", new InputStreamResource(stream));

		File file = new File("/tmp/" + ID + ".xml");

                try (FileWriter f2 = new FileWriter(file, false)) {
                    f2.write(dsa);
                }
		// Files.write(dsa, file, StandardCharsets.UTF_8);
		file.setExecutable(true, false);
		file.setReadable(true, false);
		file.setWritable(true, false);
		parts.add("file", new FileSystemResource(file));

		ResponseEntity<String> response = template.postForEntity(uri, parts,
				String.class);
		// asd.delete();
		if (response.getStatusCode() == HttpStatus.OK) {
			log.info("Success " + response.getBody());
			return true;
		} else {
			log.error("Error!");
			return false;
		}
	}

	public static boolean ChangeDSAStatus(String dsaid, String newstatus,String endpoint) throws Exception {
		String uri = endpoint+"/updatestatus";
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
		parts.add("dsaid", dsaid);
		parts.add("status", newstatus);

		ResponseEntity<String> response = template.postForEntity(uri, parts,
				String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			log.info("Success " + response.getBody());
			return true;
		} else {
			log.error("Error!" + response.getBody());
			return false;
		}
	}
	/*
	 * public static boolean UploadMappedDSA(String dsa,String ID){ String uri =
	 * "http://testcocodsa.iit.cnr.it:8080/DSAAPI/updatedsa?dsaid="+ID; //
	 * MultiValueMap<String, Object> parts = new
	 * LinkedMultiValueMap<String,Object>(); // HttpHeaders xmlHeaders = new
	 * HttpHeaders(); // xmlHeaders.setContentType(MediaType.APPLICATION_XML);
	 * // HttpEntity<Resource> xmlEntity = new HttpEntity<Resource>(dsa,
	 * xmlHeaders); // parts.add("file", dsa); RestTemplate restTemplate = new
	 * RestTemplate(); // ResponseEntity<String>
	 * las=(ResponseEntity)restTemplate.postForEntity(uri, parts, String.class);
	 * // System.out.println(las.toString()); //set interceptors/requestFactory
	 * ClientHttpRequestInterceptor ri = new LoggingRequestInterceptor();
	 * List<ClientHttpRequestInterceptor> ris = new
	 * ArrayList<ClientHttpRequestInterceptor>(); ris.add(ri);
	 * restTemplate.setInterceptors(ris); restTemplate.setRequestFactory(new
	 * BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
	 * 
	 * List<HttpMessageConverter<?>> messageConverters = new
	 * ArrayList<HttpMessageConverter<?>>(); messageConverters.add(new
	 * SourceHttpMessageConverter()); messageConverters.add(new
	 * FormHttpMessageConverter()); messageConverters.add(new
	 * StringHttpMessageConverter());
	 * restTemplate.setMessageConverters(messageConverters);
	 * 
	 * LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
	 * map.add("file", dsa); HttpHeaders headers = new HttpHeaders();
	 * headers.setContentType(MediaType.APPLICATION_XML);
	 * 
	 * HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new
	 * HttpEntity<LinkedMultiValueMap<String, Object>>( map, headers);
	 * ResponseEntity<String> result = restTemplate.exchange(uri,
	 * HttpMethod.POST, requestEntity, String.class);
	 * 
	 * System.out.println(result.toString());
	 * 
	 * 
	 * return true; }
	 */
}
