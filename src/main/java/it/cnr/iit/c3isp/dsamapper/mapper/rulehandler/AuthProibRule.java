package it.cnr.iit.c3isp.dsamapper.mapper.rulehandler;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.wso2.balana.utils.policy.dto.ConditionElementDT0;
import org.wso2.balana.utils.policy.dto.ObligationElementDTO;
import org.wso2.balana.utils.policy.dto.RuleElementDTO;
import org.wso2.balana.utils.policy.dto.TargetElementDTO;

import it.cnr.iit.c3isp.dsamapper.mapper.CommonUtils;
import it.cnr.iit.c3isp.dsamapper.mapper.DatumException;
import java.io.UnsupportedEncodingException;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.wso2.balana.utils.exception.PolicyBuilderException;

public class AuthProibRule extends AbstractRule {
	final static Logger log = LogManager.getLogger();

	public AuthProibRule(Node thisnode, RuleType ruletypew, String purposew, String dataclassificationw)
			throws RuleException, DatumException {

		log.debug("creating new AuthProibRule ");

		this.internalState = InternalState.NOT_VALID_SKIP;
		this.original_dom_parent_element = (Element) thisnode;
		this.purpose = purposew;
		this.dataClassification = dataclassificationw;
		this.ruleType = ruletypew;
		this.finalMappedRule = getXACMLMappedRule();
		this.internalState = InternalState.VALID_MAPPED;

	}

	@Override
	protected RuleElementDTO getXACMLMappedRule() throws RuleException, DatumException {
		log.debug("converting AuthProibRule..");
		if (original_dom_parent_element == null)
			throw new RuleException("no original_cnl_rule_dom");

		initCnlFromDomElement();
		RuleElementDTO mappedRule = new RuleElementDTO();

		/*
		 * Qui si fa un adattamento al nuovo caso c3isp ovvero se nella regola c'e' come
		 * attributo sul dom originale della regola stessa "statementInfo" allora va
		 * aggiunta un obligation alla regola dove c'e' un prefisso DMO seguito dalla
		 * versione url encoded di cosa si trova dentro l'attributo stesso
		 * 
		 */

		String statinfo = original_dom_parent_element.getAttribute("statementInfo");

		if (statinfo.length() > 3) { // 3 semplicemente per evitare la stringa vuota o il def value

			log.debug("Ok the rule has statmentInfo, lets create an Obligation");
			log.debug("Content of statement info: " + statinfo);
                        try {
                            mappedRule.setObligationElementDTOs(this.getObligationStatmentinfo(statinfo));
                        } catch(UnsupportedEncodingException e) {
                            log.error("UnsupportedEncoding Exception: " + e.getMessage());
                        }
			log.debug("Obligation added.");
		}

		if (this.collapseTerms()) {

			// prendi la parte target
			// prendi la parte conditionale
			// se la regola e' permit fai permit, senno fai deny, metti il nome
			// alla regola.

			ConditionElementDT0 cond = createXACMLConditionPart(true);

			String tripleteAction = getCollapsedActionPart(collapsedTerms_rule_string);
			String action = getMatchActionFromActionPart(tripleteAction);

			TargetElementDTO tar = createXACMLTargetActionPart(action);
			mappedRule.setConditionElementDT0(cond);
			mappedRule.setTargetElementDTO(tar);
			mappedRule.setRuleEffect((this.ruleType == RuleType.AUTH) ? "Permit" : "Deny");
			mappedRule.setRuleId(this.NameOftheRule);
		} else {
			log.debug(
					"Collapsed term return false. so nothing to collapse. Maybe a rule with can referring to the system or malformed dsa. ie. can [Subject,delete,Data] must be if hasId etc. Im creating a basic rule form instaed of skip and crossfingers.");
			String tripleteAction = getCollapsedActionPart(collapsedTerms_rule_string);
			String action = getMatchActionFromActionPart(tripleteAction);
			TargetElementDTO tar = createXACMLTargetActionPart(action);
			mappedRule.setTargetElementDTO(tar);
			mappedRule.setConditionElementDT0(createXACMLConditionPart(false));
			mappedRule.setRuleEffect((this.ruleType == RuleType.AUTH) ? "Permit" : "Deny");
			mappedRule.setRuleId(this.NameOftheRule);
			
		}
		return mappedRule;
	}

	@Override
	public Element getDomFinalRule() throws RuleException {
		if (this.internalState != InternalState.VALID_MAPPED) {

			throw new RuleException("this Rule it's not valid...how do you can get DOM?");
		}
                
                try {
                    Element upolElement = CommonUtils.convertXacmlRuleElementToElement(this.finalMappedRule,
                                    this.original_dom_parent_element.getOwnerDocument());

                    CommonUtils.adjustDecisionTimeAndNsAndPatchPasteONGOING(upolElement, this);

                    return upolElement;
                } catch(PolicyBuilderException e) {
                    throw new RuleException("Policy builder exception: " + e.getMessage());
                }

	}

	
}
